﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.V4.Pages.Account.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ThirdASPNETCore.Data;
using ThirdASPNETCore.Models;

namespace ThirdASPNETCore.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public AccountController(
            UserManager<IdentityUser> userManager,
            SignInManager<IdentityUser> sigInManager,
            RoleManager<IdentityRole> roleManager
        )
        {
            _userManager = userManager;
            _signInManager = sigInManager;
            _roleManager = roleManager;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Register()
        {
            return View(new RegisterViewModel());
        }

        [HttpPost]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            var user = new IdentityUser
            {
                UserName = model.UserName,
                Email = model.Email
            };
            var result = await _userManager.CreateAsync(user, model.Password);
            var resultRole = await _userManager.AddToRoleAsync(user, "User");
            if (result.Succeeded)
            {
                return RedirectToAction(nameof(Login));
            }

            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View(new LogInViewModel(Request.Query["ReturnUrl"]));
        }

        [HttpPost]
        public async Task<ActionResult> Login(LogInViewModel model)
        {
            var result = await _signInManager
                .PasswordSignInAsync(
                    model.UserName, 
                    model.Password, 
                    model.RememberMe, 
                    false
                );
            if (result.Succeeded)
                return Redirect(
                    string.IsNullOrEmpty(model.ReturnUrl)
                        ? Url.Action("Index", "Home")
                        : model.ReturnUrl
                );

            return View();
        }

        public async Task<ActionResult> Logout()
        {
            if(User.Identity.IsAuthenticated)
                await _signInManager.SignOutAsync();
            return RedirectToAction("Login", "Account");
        }

        [HttpGet]
        public ActionResult AccessDenied()
        {
            return View();
        }

        [HttpGet]
        public async Task<ActionResult> AssignRole()
        {
            var users = _userManager.Users.ToList();
            var roles = _roleManager.Roles.Select(x => x.Name).ToList();
            var model = new AssignRoleViewModel
            {
                Roles = roles,
                UsersWithRole = new List<AssignRowRoleViewModel>()
            };

            foreach (var identityUser in users)
            {
                var userRoles = string.Join(
                    ";", 
                    (await _userManager.GetRolesAsync(identityUser)).ToList()
                );
                model.UsersWithRole.Add(new AssignRowRoleViewModel
                {
                    UserId = identityUser.Id,
                    UserName = identityUser.UserName,
                    Email = identityUser.Email,
                    Roles = userRoles
                });
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> AssignRole(string UserId, string RoleName)
        {
            var user = await _userManager.FindByIdAsync(UserId);
            var roles = _roleManager.Roles.Select(x => x.Name).ToList();

            await _userManager.RemoveFromRolesAsync(user, roles);
            await _userManager.AddToRoleAsync(user, RoleName);

            return RedirectToAction(nameof(AssignRole));
        }
    }
}