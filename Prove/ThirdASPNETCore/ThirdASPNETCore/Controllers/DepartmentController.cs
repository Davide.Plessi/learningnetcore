﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using ThirdASPNETCore.Data;
using ThirdASPNETCore.Models;

namespace ThirdASPNETCore.Controllers
{
    [Authorize]
    public class DepartmentController : Controller
    {
        private readonly Repository _repository;

        public DepartmentController(Repository repository)
        {
            _repository = repository;
        }

        // GET: Department
        [Authorize(Roles = "Admin, User")]
        public ActionResult Index()
        {
            var entities = _repository.Set<Department>().ToList();
            return View(entities);
        }

        // GET: Department/Details/5
        public ActionResult Details(int id)
        {
            var entity = _repository.Set<Department>().Find(id);
            return View(entity);
        }

        // GET: Department/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            var entity = new Department();
            return View(entity);
        }

        // POST: Department/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Department entity)
        {
            try
            {
                if (!ModelState.IsValid) return View();
                _repository.Add(entity);
                _repository.SaveChanges();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception exc)
            {
                TempData["Exception"] = exc.Message;
                return View();
            }
        }

        // GET: Department/Edit/5
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var entity = _repository.Set<Department>().Find(id);
            return View(entity);
        }

        // POST: Department/Edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Department entity)
        {
            try
            {
                if (!ModelState.IsValid) return View();
                _repository.Update(entity);
                _repository.SaveChanges();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception exc)
            {
                TempData["Exception"] = exc.Message;
                return View();
            }
        }

        public ActionResult Delete(int id)
        {
            try
            {
                _repository.Remove(new Department {Id = id});
                _repository.SaveChanges();
            }
            catch (Exception exc)
            {
                TempData["Exception"] = exc.Message;
            }
            return RedirectToAction(nameof(Index));
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        _repository.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}