﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ThirdASPNETCore.Models
{
    public class RestrictedValue : ValidationAttribute
    {
        public List<string> ForbiddenValues { get; set; }
        public RestrictedValue(string[] forbiddenValues, string errorMessage = null) : base()
        {
            ForbiddenValues = forbiddenValues?.ToList() ?? new List<string>();
            ErrorMessage = errorMessage ?? "Forbidden value";
        }
        protected override ValidationResult IsValid(
            object value, 
            ValidationContext validationContext
        )
        {
            return ForbiddenValues.Contains(value) 
                ? new ValidationResult(ErrorMessage) 
                : ValidationResult.Success;
        }
    }

    [Table("Departments")]
    public class Department
    {
        [Key]
        [Column("Id")]
        public int Id { get; set; }

        [Column("Name")]
        [RestrictedValue(new []{ "Prodotti" })]
        [Required(ErrorMessage = "Name is required!")]
        [MaxLength(20, ErrorMessage = "Maximum length of 20 characters exceeded!")]
        public string Name { get; set; }

        [Column("Description")]
        [MaxLength(50, ErrorMessage = "Maximum length of 50 characters exceeded!")]
        [DefaultValue("No description")]
        public string Description { get; set; }

        public virtual List<Employee> Employees { get; set; }

        public virtual List<CustomerDepartment> CustomerDepartments { get; set; }
    }
}
