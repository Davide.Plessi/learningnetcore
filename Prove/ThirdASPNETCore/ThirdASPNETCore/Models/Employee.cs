﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ThirdASPNETCore.Models
{
    [Table("Employees")]
    public class Employee
    {
        [Key]
        [Column("Id")]
        public int Id { get; set; }

        [Column("Name")]
        [Required]
        [MaxLength(20)]
        public string Name { get; set; }

        [Column("Description")]
        [MaxLength(50)]
        public string Description { get; set; }

        [ForeignKey("Department")]
        [Column("DepartmentId")]
        public int DepartmentId { get; set; }
        public virtual Department Department { get; set; }
    }
}
