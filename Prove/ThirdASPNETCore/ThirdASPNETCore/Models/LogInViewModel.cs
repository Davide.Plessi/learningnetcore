﻿using System.ComponentModel.DataAnnotations;

namespace ThirdASPNETCore.Models
{
    public class LogInViewModel
    {
        [Display(Name = "User name")]
        [Required]
        public string UserName { get; set; }

        [Required]
        public string Password { get; set; }

        [Display(Name = "Remember me")]
        public bool RememberMe { get; set; }

        public string ReturnUrl { get; set; }

        public LogInViewModel() { }

        public LogInViewModel(string returnUrl)
        {
            ReturnUrl = returnUrl;
        }
    }
}
