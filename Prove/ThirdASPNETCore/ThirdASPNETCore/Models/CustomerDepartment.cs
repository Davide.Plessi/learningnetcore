﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ThirdASPNETCore.Models
{
    [Table("CustomerDepartment")]
    public class CustomerDepartment
    {
        [Key]
        [Column("Id")]
        public int Id { get; set; }

        [Column("CustomerId")]
        public string CustomerId { get; set; }
        public virtual Customer Customer { get; set; }

        [Column("DepartmentId")]
        public string DepartmentId { get; set; }
        public virtual Department Department { get; set; }
    }
}
