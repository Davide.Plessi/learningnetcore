﻿using System.Collections.Generic;

namespace ThirdASPNETCore.Models
{
    public class AssignRoleViewModel
    {
        public List<AssignRowRoleViewModel> UsersWithRole { get; set; }
        public List<string> Roles { get; set; }
    }

    public class AssignRowRoleViewModel
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Roles { get; set; }
    }
}
