﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ThirdASPNETCore.Models
{

    [Table("Customer")]
    public class Customer
    {
        [Key]
        [Column("Id")]
        public int Id { get; set; }

        [Column("Name")]
        [Required]
        [MaxLength(20)]
        public string Name { get; set; }

        [Column("Surname")]
        [MaxLength(20)]
        public string Surname { get; set; }

        public virtual List<CustomerDepartment> CustomerDepartments { get; set; }
    }
}
