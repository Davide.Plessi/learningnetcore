﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EFCoreClassDesignTechEg
{
    class ClassStandard
    {
        [Key]
        public int ClassId { get; set; }
        public string ClassName { get; set; }
        public string Description { get; set; }
        public List<Student> Students { get; set; }
    }
    class Student
    {
        [Key]
        public int StudentId { get; set; }
        public string StudentName { get; set; }
        public string Gender { get; set; }
        public int ClassId { get; set; }
        public ClassStandard ClassStandard { get; set; }
        public List<StudentCourse> Student_Courses { get; set; }
        public List<Exam> Exams { get; set; }
    }
    class Course
    {
        [Key]
        public int CourseId { get; set; }
        public string CourseName { get; set; }
        public string Description { get; set; }
        public List<StudentCourse> Student_Courses { get; set; }
        public List<Exam> Exams { get; set; }
    }
    class StudentCourse
    {
        [Key]
        public int SCid { get; set; }
        public int StudentId { get; set; }
        public int CourseId { get; set; }
        public Student Student { get; set; }
        public Course Course { get; set; }
    }
    class Exam
    {
        [Key]
        public int ExamId { get; set; }
        public int StudentId { get; set; }
        public int CourseId { get; set; }
        public DateTime DOE { get; set; }
        public double Marks { get; set; }
        public Student Student { get; set; }
        public Course Course { get; set; }
    }
    class SchoolDbContext : DbContext
    {
        private const string ConnectionString =
            "Data Source=10.0.75.1,1433;Initial Catalog=EFCoreSchool;Persist Security " +
            "Info=True;User ID=SA;Password=sa2016!NSI";

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(ConnectionString);
        }

        public DbSet<ClassStandard> ClassStandards { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<StudentCourse> Student_Courses { get; set; }
        public DbSet<Exam> Exams { get; set; }
    }
    class Program
    {
        static void Main(string[] args)
        {

        }
    }
}
