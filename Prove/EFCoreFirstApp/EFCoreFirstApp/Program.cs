﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace EFCoreFirstApp
{
    class Department
    {
        [Key]
        public int DepartmentId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime DateOfFormation { get; set; }
        
        //Relationship with employee property 1:M
        //public List<Employee> Employees { get; set; }
        
        //Relationship properties M:M
        public List<EmployeeDepartment> EmployeeDepartments { get; set; }
    }
    
    class Employee
    {
        [Key]
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public float Salary { get; set; }
        
        //Relationship properties 1:M
        //public int DepartmentId { get; set; }
        //public Department Department { get; set; }
        
        //Relationship properties M:M
        public List<EmployeeDepartment> EmployeeDepartments { get; set; }
    }

    class EmployeeDepartment
    {
        public int EmployeeDepartmentId { get; set; }
        
        public int EmployeeId { get; set; }
        public Employee Employee { get; set; }
        
        public int DepartmentId { get; set; }
        public Department Department { get; set; }
    }
    
    class OrganizationContext:DbContext
    {
        private static ILoggerFactory GetLoggerFactory()
        {
            IServiceCollection serviceCollection = new ServiceCollection();
            serviceCollection.AddLogging(builder =>
                builder
                    .AddConsole()
                    .AddFilter(DbLoggerCategory.Database.Command.Name, LogLevel.Debug));
            return serviceCollection.BuildServiceProvider()
                .GetService<ILoggerFactory>();
        }
        private const string ConnectionString =
            "Data Source=10.0.75.1,1433;Initial Catalog=EFCoreFirstApp;Persist Security " +
            "Info=True;User ID=SA;Password=sa2016!NSI";
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseLoggerFactory(GetLoggerFactory())
                .UseSqlServer(ConnectionString);
        }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Employee> Employees { get; set; }
    }
    
    class Program
    {
        static void Main(string[] args)
        {
            var context = new OrganizationContext();
            
            #region Insert new 
            var department = new Department
            {
                //DepartmentId auto-increment
                Name = "Dev",
                Description = "Roba"
            };
            context.Departments.Add(department);
            context.SaveChanges();
            #endregion
            
            #region Select all
            var allDepartments = context.Departments.ToList();
            #endregion

        }
    }
}
