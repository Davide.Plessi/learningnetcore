﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace FirstStaticMVCCore.Controllers
{
    public class EmployeeController : Controller
    {
        public ContentResult Index()
        {
            //return "boborobo";
            var html = "<h1> SBT - From home index </h1>";
            return Content(content: html, contentType: "text/html");
        }
    }
}