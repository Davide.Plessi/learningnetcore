﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.SqlClient;
using System.Linq;

namespace DataAnnotationsEg
{
    [Table("Author")]
    class Author
    {
        [Key]
        public int AuthorId { get; set; }

        [MaxLength(15)]
        [Required]
        public string FirstName { get; set; }

        [MaxLength(15), Required]
        public string LastName { get; set; }

        [NotMapped]
        public string FullName
        {
            get { return FirstName + "" + LastName; }
        }

        public List<Book> Books { get; set; }
    }

    [Table("Book")]
    class Book
    {
        [Key, Column(Order = 0)]
        public int BookId { get; set; }

        [Column(Order = 1)]
        [Required]
        public string BookName { get; set; }

        [Column(Order = 2)]
        [ConcurrencyCheck]
        [Required]
        public double PricePerUnit { get; set; }

        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreatedOn { get; set; }

        [Column(Order = 4)]
        [ForeignKey("Author")]
        public int AuthorId { get; set; }

        public Author Author { get; set; }
    }

    [NotMapped]
    class Test
    {
        [Key]
        public int TestId { get; set; }
    }

    class ShopDbContext : DbContext
    {
        private const string ConnectionString = "";
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(ConnectionString);
            //optionsBuilder.UseLazyLoadingProxies();
        }
        public DbSet<Author> Authors { get; set; }
        public DbSet<Book> Books { get; set; }

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    modelBuilder
        //        .Entity<Book>()
        //        .HasOne(x => x.Author)
        //        .WithMany(x => x.Books);
        //}
    }

    class Program
    {
        static void Main(string[] args)
        {
            #region Insert
            //var shopDbContext = new ShopDbContext();
            //var author = new Author() { FirstName = "Manzoor", LastName = "Ahmed" };

            ////shopDbContext.Authors.Add(author);
            ////shopDbContext.SaveChanges();

            //shopDbContext.Add<Author>(author);
            //shopDbContext.SaveChanges();
            #endregion

            //it is good and wise dispose the context object after using it
            //shopDbContext.Dispose();

            #region Delete
            //using (ShopDbContext shopDbContext = new ShopDbContext())
            //{
            //    var author = shopDbContext
            //        .Authors
            //        .FirstOrDefault(x => x.AuthorId == 4);

            //    //shopDbContext.Authors.Remove(author);
            //    //shopDbContext.SaveChanges();

            //    shopDbContext.Remove<Author>(author);
            //    shopDbContext.SaveChanges();
            //}
            #endregion

            #region Update
            ////Connected approach
            //using (var shopDbContext = new ShopDbContext())
            //{
            //    var author = shopDbContext
            //      .Authors
            //      .FirstOrDefault(x => x.AuthorId == 3);
            //    author.FirstName = "Jack";
            //    shopDbContext.Update<Author>(author);
            //    shopDbContext.SaveChanges();
            //}
            #endregion

            #region Select
            Console.Write("Enter Book Id : ");
            var bookId = int.Parse(Console.ReadLine());

            using (var shopDbContext = new ShopDbContext())
            {
                //By primary key
                var book = shopDbContext.Books.Find(bookId);

                //The first that sadisfy the expression
                book = shopDbContext
                    .Books
                    .FirstOrDefault(x => Math.Abs(x.PricePerUnit - 1373) < 0.001); //return the first or null
                book = shopDbContext
                    .Books
                    .SingleOrDefault(x => Math.Abs(x.PricePerUnit - 1373) < 0.001); //if multiple row throw an exception
                if (book != null)
                {
                    Console.WriteLine("BookId:{0} Name:{1} Price:{2}"
                        , book.BookId, book.BookName, book.PricePerUnit);
                }
                else
                {
                    Console.WriteLine("Book Not Found");
                }

                //select count(*) from Book
                var numberOfBooks = shopDbContext.Books.Count();
                Console.WriteLine("Number Of Books are " + numberOfBooks);

                //PricePerUnit>5000
                var list2 = shopDbContext.Books.Where(x => x.PricePerUnit > 5000).ToList();
                var list3 = shopDbContext.Books.Where(x => x.AuthorId == 3).ToList();

                //Get All the books whose author's first name is "Manzoor"
                var authorId = shopDbContext.Authors.FirstOrDefault(x => x.FirstName == "Manzoor")?.AuthorId;
                var list4 = shopDbContext.Books.Where(x => x.AuthorId == authorId).ToList();

                var list = shopDbContext.Books.Where(x => x.Author.FirstName == "Manzoor").ToList();

                foreach (var bookElement in list)
                {
                    Console.WriteLine("BookId:{0} Name:{1} Price:{2}", bookElement.BookId, bookElement.BookName, bookElement.PricePerUnit);
                }

                Console.ReadLine();
            }
            #endregion

        }

        //Disconnected approach
        //private ShopDbContext Context;

        //public void OnLoad()
        //{
        //    Context = new ShopDbContext();
        //}
        //public void Update(Author author)
        //{
        //    //Context.Authors.Update(author);
        //    //Context.SaveChanges();
        //    //OR
        //    Context.Update<Author>(author);
        //    Context.SaveChanges();
        //}
    }
}
