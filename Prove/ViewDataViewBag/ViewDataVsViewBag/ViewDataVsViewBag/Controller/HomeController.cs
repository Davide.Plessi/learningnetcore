﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace ViewDataVsViewBag.Controller
{
    public class HomeController : Microsoft.AspNetCore.Mvc.Controller
    {
        public IActionResult Index()
        {
            #region VdVsVb
            //ViewData["MyData"] = 25;
            //ViewBag.MyData = 25; 
            #endregion

            return View();
        }
    }
}