﻿using Entity;
using Interfaces;
using Services;
using System;

namespace FinalEFConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var repository = new Repository();
            ICommessaService commessaService = new CommessaService(repository);
            IUtenteService utenteService = new UtenteService(repository);
            IIntervalloTemporaleService intervalloTemporaleService = new IntervalloTemporaleService(repository);

            #region Inserimento
            //var nuovoUtente = new Utente
            //{
            //    Cognome = "Io",
            //    Nome = "SempreIo",
            //    NomeArea = "Cazzeggio"
            //};
            //utenteService.PrepareForSaving(nuovoUtente);

            //var nuovaCommessa = new Commessa
            //{
            //    Nome = "Sviluppo",
            //    Codice = "SC",
            //    NomeCliente = "Roba"
            //};
            //commessaService.PrepareForSaving(nuovaCommessa);

            //var nuovoIntervalloTemporale = new IntervalloTemporale()
            //{
            //    Commessa = nuovaCommessa,
            //    Utente = nuovoUtente,
            //    DataInizio = DateTime.Now,
            //    DataFine = DateTime.Now.AddHours(1)
            //};
            //intervalloTemporaleService.PrepareForSaving(nuovoIntervalloTemporale);

            //repository.SaveChanges(); 
            #endregion


            #region Lettura singola con lazy loading
            //var ultimaTimbratura = intervalloTemporaleService.GetLastIntervalloTemporale();

            //Console.WriteLine($"{ultimaTimbratura.Commessa?.Nome}: {ultimaTimbratura.DataFine:d} " +
            //    $"{ultimaTimbratura.DataInizio:HH:mm}-{ultimaTimbratura.DataFine:HH:mm} ");
            //Console.WriteLine($"Fatta da {ultimaTimbratura.Utente.Cognome} {ultimaTimbratura.Utente.Nome}"); 
            #endregion

            #region Lettura lista con Eager Loading
            //var listaTimbrature = intervalloTemporaleService.LoadAll(x => true);
            //foreach (var intervalloTemporale in listaTimbrature)
            //    StampaTimbratura(intervalloTemporale);
            #endregion

            #region Calcolo ore giornaliere

            //const int utenteId = 1;
            ////var utente = utenteService.LoadSingle(utenteId);
            //var giorniOrePerUtente = intervalloTemporaleService
            //    .GetOreGiornalierePerUtente(DateTime.MinValue, DateTime.MaxValue, utenteId);
            //foreach (var giorno in giorniOrePerUtente)
            //    StampaOreGiorno(giorno);
            #endregion

        }

        public static void StampaTimbratura(IntervalloTemporale intervallo)
        {
            Console.WriteLine($"{intervallo.Commessa?.Nome}: {intervallo.DataFine:d} " +
                $"{intervallo.DataInizio:HH:mm}-{intervallo.DataFine:HH:mm} ");
            Console.WriteLine($"Fatta da {intervallo.Utente?.Cognome} {intervallo.Utente?.Nome}");
        }

        public static void StampaOreGiorno(OreGiorno oreGiorno)
        {
            Console.WriteLine($"{oreGiorno.Giorno:d}: {oreGiorno.Ore:hh\\:mm}");
        }
    }
}
