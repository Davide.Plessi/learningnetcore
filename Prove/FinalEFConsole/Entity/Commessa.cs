﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entity
{
    [Table("Commesse")]
    public class Commessa : Entity
    {
        [Column("Nome")]
        public string Nome { get; set; }

        [Column("NomeCliente")]
        public string NomeCliente { get; set; }

        [Column("Codice")]
        public string Codice { get; set; }
    }
}
