﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity
{
    public class IntervalloTemporale : Entity
    {
        [ForeignKey("Utente")]
        [Column("UtenteId")]
        public int? UtenteId { get; set; }

        public virtual Utente Utente { get; set; }

        [ForeignKey("Commessa")]
        [Column("CommessaId")]
        public int? CommessaId { get; set; }

        public virtual Commessa Commessa { get; set; }

        [Column("DataInizio")]
        public DateTime DataInizio { get; set; }

        [Column("DataFine")]
        public DateTime? DataFine { get; set; }
    }

    [NotMapped]
    public class OreGiorno
    {
        public TimeSpan Ore { get; set; }
        public DateTime Giorno { get; set; }
    }
}
