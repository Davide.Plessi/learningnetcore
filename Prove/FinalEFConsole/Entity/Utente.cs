﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entity
{
    public class Utente : Entity
    {
        [Column("Nome")]
        public string Nome { get; set; }

        [Column("Cognome")]
        public string Cognome { get; set; }
        
        [Column("NomeArea")]
        public string NomeArea { get; set; }
    }
}
