﻿using System.ComponentModel.DataAnnotations;

namespace Entity
{
    public abstract class Entity
    {
        [Key]
        public int Id { get; set; }
    }
}
