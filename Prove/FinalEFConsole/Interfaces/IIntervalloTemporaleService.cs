﻿using System;
using System.Collections.Generic;
using Entity;

namespace Interfaces
{
    public interface IIntervalloTemporaleService : ICrudService<IntervalloTemporale>
    {
        IntervalloTemporale GetLastIntervalloTemporale();
        List<OreGiorno> GetOreGiornalierePerUtente(DateTime? dataInizio, DateTime? dataFine, int utenteId);
    }
}
