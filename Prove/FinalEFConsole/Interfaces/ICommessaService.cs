﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Interfaces
{
    public interface ICrudService<T>
        where T : Entity.Entity, new()
    {
        int Save(T o, bool forceInsert = false);

        List<T> LoadAll(Expression<Func<T, bool>> filter);

        T LoadSingle(int id);

        T LoadSingle(Expression<Func<T, bool>> filter);

        long Delete(int id);

        List<TResult> LoadAndSelect<TResult>(
            Expression<Func<T, bool>> filter,
            Expression<Func<T, TResult>> selector,
            bool distinct
        );

        void PrepareForSaving(T o, bool forceInsert = false);
    }
}
