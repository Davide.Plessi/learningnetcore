﻿using Entity;

namespace Interfaces
{
    public interface ICommessaService : ICrudService<Commessa>
    {
    }
}
