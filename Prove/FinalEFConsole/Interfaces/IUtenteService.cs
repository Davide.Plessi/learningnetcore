﻿using Entity;

namespace Interfaces
{
    public interface IUtenteService : ICrudService<Utente>
    {
    }
}
