﻿using System;
using System.Collections.Generic;
using System.Text;
using Entity;
using Microsoft.EntityFrameworkCore;

namespace Services
{
    public class UtenteService : CrudService<Utente, Repository>, Interfaces.IUtenteService
    {
        public UtenteService(DbContext dbContext) : base(dbContext)
        {
        }
    }
}
