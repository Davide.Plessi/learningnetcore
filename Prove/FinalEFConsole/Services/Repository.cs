﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Entity;

namespace Services
{
    public class Repository : DbContext
    {
        private const string ConnectionString =
            "Data Source=10.0.75.1,1433;Initial Catalog=FinalEfConsole;Persist Security " +
            "Info=True;User ID=SA;Password=sa2016!NSI";

        private static ILoggerFactory GetLoggerFactory()
        {
            IServiceCollection serviceCollection = new ServiceCollection();
            serviceCollection.AddLogging(builder =>
                builder
                    .AddDebug()
                    .AddFilter(DbLoggerCategory.Database.Command.Name, LogLevel.Information));
            return serviceCollection.BuildServiceProvider()
                .GetService<ILoggerFactory>();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseLoggerFactory(GetLoggerFactory())
                .UseLazyLoadingProxies()
                .UseSqlServer(ConnectionString);
        }

        public DbSet<Commessa> Commesse { get; set; }
        public DbSet<Utente> Utenti { get; set; }
        public DbSet<IntervalloTemporale> IntervalliTemporali { get; set; }

    }
}
