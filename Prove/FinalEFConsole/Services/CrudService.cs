﻿using Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Services
{
    public abstract class CrudService<T, TDbContext> : BaseService<DbContext>, ICrudService<T>
        where T : Entity.Entity, new()
        where TDbContext : DbContext
    {
        protected CrudService(DbContext dbContext) : base(dbContext)
        {
        }

        public DbSet<T> DbSet => DbContext.Set<T>();

        protected virtual IQueryable<T> PrepareSet(IQueryable<T> input) => input;

        public void PrepareForSaving(T o, bool forceInsert = false)
        {
            DbContext.Entry(o).State = forceInsert || o.Id == 0
                ? EntityState.Added
                : EntityState.Modified;
        }

        public int Save(T o, bool forceInsert = false)
        { 
            return o.Id;
        }

        public List<T> LoadAll(Expression<Func<T, bool>> filter)
        {
            var set = PrepareSet(DbSet);
            var entities = set.Where(filter).ToList();
            return entities;
        }

        public T LoadSingle(int id)
        {
            var set = PrepareSet(DbSet);
            var entity = set.FirstOrDefault(x => x.Id == id);
            return entity;
        }

        public T LoadSingle(Expression<Func<T, bool>> filter)
        {
            var set = PrepareSet(DbSet);
            var entity = set.FirstOrDefault(filter);
            return entity;
        }

        public long Delete(int id)
        {
            DbContext.Set<T>().Remove(new T { Id = id });
            DbContext.SaveChanges();
            return id;
        }

        public List<TResult> LoadAndSelect<TResult>(
            Expression<Func<T, bool>> filter, 
            Expression<Func<T, TResult>> selector, bool distinct
        )
        {
            IQueryable<T> set = PrepareSet(DbSet);
            if (filter != null)
                set = set.Where(filter);

            var list = set.Select(selector);
            if (distinct)
                list = list.Distinct();

            return list.ToList();
        }
    }
}
