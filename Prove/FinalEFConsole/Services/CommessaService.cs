﻿using System;
using System.Collections.Generic;
using System.Text;
using Entity;
using Microsoft.EntityFrameworkCore;

namespace Services
{
    public class CommessaService : CrudService<Commessa, Repository>, Interfaces.ICommessaService
    {
        public CommessaService(DbContext dbContext) : base(dbContext)
        {
        }
    }
}
