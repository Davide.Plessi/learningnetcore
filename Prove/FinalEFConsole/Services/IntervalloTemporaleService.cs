﻿using Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Services
{
    public class IntervalloTemporaleService : CrudService<IntervalloTemporale, Repository>, Interfaces.IIntervalloTemporaleService
    {
        public IntervalloTemporaleService(DbContext dbContext) : base(dbContext)
        {
        }

        protected override IQueryable<IntervalloTemporale> PrepareSet(IQueryable<IntervalloTemporale> input)
        {
            return base.PrepareSet(input)
                .Include(x => x.Commessa)
                .Include(x => x.Utente);
        }

        public IntervalloTemporale GetLastIntervalloTemporale()
        {
            return PrepareSet(DbSet)
                .FirstOrDefault(x => x.DataFine == DbContext.Set<IntervalloTemporale>()
                .Max(y => y.DataFine));
        }

        public List<OreGiorno> GetOreGiornalierePerUtente(DateTime? dataInizio, DateTime? dataFine, int utenteId)
        {
            dataInizio = dataInizio ?? DateTime.MinValue;
            dataFine = dataFine ?? DateTime.MaxValue;

            if (dataInizio > dataFine)
                throw new Exception("Non scrivere stronzate");

            var result = DbSet.Where(x => x.UtenteId == utenteId && 
                x.DataFine.HasValue &&
                x.DataFine.Value.Date <= dataFine &&
                x.DataInizio.Date >= dataInizio)
                .ToList()
                .GroupBy(x => x.DataInizio.Date)
                .Select(x => new OreGiorno
                {
                    Giorno = x.Key,
                    Ore = x.Select(y => y.DataFine.Value - y.DataInizio)
                        .Aggregate(new TimeSpan(), (current, next) => current + next)
                })
                .ToList();
            return result;
        }
    }
}
