# What is EF
Entity Framework is the latest technology provided by ADO.net for accessinng database.  
- It is an ORM tool.  
- It provides easy and automated way of mapping between Classes and Relations.  
- It auto generates most of the data-access code.

# Why use EF
It provides full intelliSense support for columns names and table names

# What is EF Core
- EF Core is an ORM tool
- EF Core has almost same interfaces as EF6.X
- EF Core can be used to access NpSQL DB. Where EF6.X cannot
- EF Core can run on Windows, Linux and Apple. Where as EF6.X is only for Windows.
- There are two approaches of working with EF Core
    * Code-First: generate database tables from classes
    * Database-First (Reverse Engineering): generate classes from database tables

## Code-First
![CodeFirstExample](img/CodeFirstExample.jpg)
In the **Code-First** approach, you focus on your application and start creating classes for it and we cal them as business entities or business objects

# Getting started with EF Core
- **Start a new .Net Core Console App**: New project -> .Net Core -> Console App
- **Install -Nuget Packages**: Microsoft.EntityFrameworkCore.SqlServer: Right click on Project (Solution Explorer) -> manage nugetPackages -> search package -> Install
- **Create an Entity _Department_**: in the Program.cs file inside the namespace we can create our entity: 
    ```c#
    class Department
    {
        [Key]
        public int DepartmentId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
    ```
- **Create a _DbContext_ class**:  _DbContext_ is the top-level object that manages database connection and provides the functionalities on database like Create, Read, Update and Delete operations. It is our main data access class, using which we perform all the operations, should be inherited from DbContext
    ```c#
    class OrganizationContext:DbContext
    {
    }
    ```
    Our _OrganizationContext_ class should hav DbSet<TEntity> properties for each entity or object
    ```c#
    class OrganizationContext:DbContext
    {
        public DbSet<Department> Department { get; set; }
    }    
    ```
    DbSet is used to perform CRUD operations on TEntity. (see Creating and updating Database for the full code)
- **Create a new Department**: In the main method
    ```c#
    var context = new OrganizationContext();
    var department = new Department
    {
        //DepartmentId auto-increment
        Name = "Dev",
        Description = "Roba"
    };
    context.Department.Add(department);
    context.SaveChanges();
    ```
    First of all we need to initialize the context, then we can create our department.
    When we have our object we can add it to the context then commit the changes to the DB with SaveChanges
- **Select all**:
    ```c#
    var allDepartments = context.Department.ToList();
    ```
    Using ToList on the DbSet<Department> it will perform the query to get all elements in the table

## Creating and updating Database
- **Install - Nuget Package _Microsoft.EntityFrameworkCore.Tools_** (Without VS also install _Microsoft.EntityFrameworkCore.Tools.DotNet_)
- **Override _OnConfiguring()_ method in _OrganizationContext_ with connection string**:
    ```c#
    private const string ConnectionString =
        "Data Source=10.0.75.1,1433;Initial Catalog=EFCoreFirstApp;Persist Security " +
        "Info=True;User ID=SA;Password=sa2016!NSI";
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder
            .UseSqlServer(ConnectionString);
    }
    ```
- **Open Package Manager Console and run below commands**:
    ```
    add-migration <name> //build a migration with the differences between the latest migrations context state and the current context state
    update-database //apply all the migrations unapplied to the database
    ```
- **Alternative without VS - Open a terminal inside the project forlder and run**:
    ```
    dotnet ef migrations add First //like add-migration
    dotnet ef database update //like update-database
    ```
After that we can notice that it created our database with the Departments table.
It also create a new Folder **Migrations** with many files:
- 20190326114438_First.cs: contains the actions to update the database UP and DOWN (to revert changes)
- 20190326114438_First.Designer.cs: describe the context after the migration
- OrganizationContextModelSnapshot.cs: describe the actual context state

If we add a property in the Department class and another class: 
```c#
class Department
{
    [Key]
    public int DepartmentId { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public DateTime DateOfFormation { get; set; }
}
class Employee
{
    [Key]
    public int EmployeeId { get; set; }
    public string Name { get; set; }
    public string Gender { get; set; }
    public float Salary { get; set; }
}
```
and repeat the two commands to update database (add-migration, update-database), it will create two more files: 
- 20190326121429_Second.cs
- 20190326121429_Second.Designer.cs
and it will updte the OrganizationContextModelSnapshot.cs with the current context state. 

## Revert Database changes
To remove a migration first of all we need to revert the changes on the database:
```
update-database <TargetMigrationName> 
```
or 
```
dotnet ef database update <TargetMigrationName>
```
In our case:
```
update-database 20190326114438_First
```
After that we can remove it (the following command remove the latest migration and update the context snapshot)
```
remove-migration
```
or 
```
dotnet ef migrations remove
```
## How to generate .sql
```
script-migration
```
or 
```
dotnet ef migrations script
```

# Class Design Techniques
**One class for each object**
## Relationship (1:M)
For example one _Department_ can have many _Employee_ so we add the **List of Employees** in the _Department_ class, the  **DepartmentId** and the **Department** properties in the _Employee_ class
```c#
class Department
{
    [Key]
    public int DepartmentId { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public DateTime DateOfFormation { get; set; }
    
    //Relationship with employee property
    public List<Employee> Employees { get; set; }
}

class Employee
{
    [Key]
    public int EmployeeId { get; set; }
    public string Name { get; set; }
    public string Gender { get; set; }
    public float Salary { get; set; }
    
    //Relationship properties
    public int DepartmentId { get; set; }
    public Department Department { get; set; }
}
```
## Relationship (M:M)
If an _Employee_ can have many _Department_ we need to add a new Cross Table class **EmployeeDepartment**, one **List of EmployeeDepartment** in the _Department_ class and one **List of EmployeeDepartment** in the _Employee_ class:
```c#
class Department
{
    [Key]
    public int DepartmentId { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public DateTime DateOfFormation { get; set; }

    //Relationship properties M:M
    public List<EmployeeDepartment> EmployeeDepartments { get; set; }
}

class Employee
{
    [Key]
    public int EmployeeId { get; set; }
    public string Name { get; set; }
    public string Gender { get; set; }
    public float Salary { get; set; }
    
    //Relationship properties M:M
    public List<EmployeeDepartment> EmployeeDepartments { get; set; }
}

class EmployeeDepartment
{
    public int EmployeeDepartmentId { get; set; }
    
    public int EmployeeId { get; set; }
    public Employee Employee { get; set; }
    
    public int DepartmentId { get; set; }
    public Department Department { get; set; }
}
```

# Data annotations attribute
https://docs.microsoft.com/it-it/ef/ef6/modeling/code-first/data-annotations  
Data annotations redefine object adding attributes.
We have:
- **Table()**: specify table properties
- **Columnt()**: specify column properties
- **Key**: mark as primary key
- **Required**: mark as required
- **MaxLength()**: specify the max lenght
- **DatabaseGenerated()**: If you're mapping your Code First classes to tables that contain computed columns, you don't want Entity Framework to try to update those columns. But you do want EF to return those values from the database after you've inserted or updated data. You can use the DatabaseGenerated annotation to flag those properties in your class.
- **ConcurrencyCheck**: the ConcurrencyCheck annotation allows you to flag one or more properties to be used for concurrency checking in the database when a user edits or deletes an entity.The command will attempt to locate the correct row by filtering not only on the key value but also on the original value of the property with this attribute. If someone has changed the blogger name for that blog in the meantime, this update will fail and you’ll get a DbUpdateConcurrencyException that you'll need to handle.
- **NotMapped**: you can mark any properties that do not map to the database with the NotMapped annotation.
- **ForeignKey**: mark the property as foreign key

Ex.
```c#
[Table("Author")]
class Author
{
    [Key]
    public int AuthorId { get; set; }

    [MaxLength(15)]
    [Required]
    public string FirstName { get; set; }

    [MaxLength(15),Required]
    public string LastName { get; set; }

    [NotMapped]
    public string FullName
    {
        get { return FirstName + "" + LastName; }
    }

    public List<Book> Books { get; set; }
}

[Table("Book")]
class Book
{
    [Key,Column(Order =0)]
    public int BookId { get; set; }

    [Column(Order =1)]
    [Required]
    public string BookName { get; set; }

    [Column(Order = 2)]
    [ConcurrencyCheck]
    [Required]
    public double PricePerUnit { get; set; }

    [Column(Order =3)]
    [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
    public DateTime CreatedOn { get; set; }

    [Column(Order = 4)]
    [ForeignKey("Author")]
    public int AuthorId { get; set; }

    public Author Author { get; set; }
}
```

# Fluent API
http://www.entityframeworktutorial.net/efcore/fluent-api-in-entity-framework-core.aspx  
Is an alternative to DataAnnotations to set certain Database-EF properties.
To use it we have to override the OnModelCreating method int the context:
```c#
class ShopDbContext : DbContext
{
    private const string ConnectionString = "";
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlServer(ConnectionString);
    }
    public DbSet<Author> Authors { get; set; }
    public DbSet<Book> Books { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        //Do the same thing of ForeignKey annotations on AuthorId
         modelBuilder
            .Entity<Book>()
            .HasOne(x => x.Author)
            .WithMany(x => x.Books);
    }
}
```


# Query log
We need to add a log system to check the exat query that EF perform and when.  
EF Core logging currently requires an ILoggerFactory which is itself configured with one or more ILoggerProvider.
- Install nuGet : Microsoft.Extensions.Logging.Debug
- Inside the context class insert:
    ```c#
        private ILoggerFactory GetLoggerFactory()
        {
            IServiceCollection serviceCollection = new ServiceCollection();
            serviceCollection.AddLogging(builder =>
                builder
                    .AddDebug()
                    .AddFilter(DbLoggerCategory.Database.Command.Name, LogLevel.Information));
            return serviceCollection
                .BuildServiceProvider()                     
                .GetService<ILoggerFactory>()
        }
    ```
- Inside the OnConfiguring method insert:
    ```c#
        optionsBuilder
            .UseLoggerFactory(GetLoggerFactory())
            .UseSqlServer(ConnectionString);
    ```

# CRUD
_**Tip**:_
It is good and wise to dispose the context object after using it. The DbContext class take from IDisposable so it can be manually removed from memory using the .Dispose() method.
Another way to free the allocated memory is to use the **using**:
```c#
using (var shopDbContext = new ShopDbContext())
{
    var author = shopDbContext.Authors.Find(4);

    //shopDbContext.Authors.Remove(author);
    //shopDbContext.SaveChanges();
    //or
    shopDbContext.Remove<Author>(author);
    shopDbContext.SaveChanges();
}
```
## Insert
- Create an object
- Add it to the context
- Save the context changes
```c#
var author = new Author() { FirstName = "Manzoor", LastName = "Ahmed" };

//shopDbContext.Authors.Add(author);
//shopDbContext.SaveChanges();
//OR
shopDbContext.Add(author); //The set deducet from the object type passed in this case author
shopDbContext.SaveChanges();
```
## Delete
- Load the object from the database
- Remove it from the context
- Save the context changes
```c#
var author = shopDbContext
    .Authors
    .FirstOrDefault(x => x.AuthorId == 4) //Where expression;

//shopDbContext.Authors.Remove(author);
//shopDbContext.SaveChanges();
//OR
shopDbContext.Remove<Author>(author);
shopDbContext.SaveChanges();
```
## Update
### Connected approach
- Load the object from the database
- Change it
- Save the context changes
```c#
var author = shopDbContext
    .Authors
    .FirstOrDefault(x => x.AuthorId == 3);
author.FirstName = "Jack";
shopDbContext.SaveChanges();
```
### Disconnected approach
```c#
private ShopDbContext Context;

public void OnLoad()
{
    Context = new ShopDbContext();
}
public void Update(Author author)
{
    //Context.Authors.Update(author);
    //Context.SaveChanges();
    //OR
    Context.Update<Author>(author);
    Context.SaveChanges();
}
```
**The disconnected approach is also applicable to delete**
## Select
Examples:
```c#
 Console.Write("Enter Book Id : ");
var bookId = int.Parse(Console.ReadLine());

using (var shopDbContext = new ShopDbContext())
{
    //By primary key
    var book = shopDbContext.Books.Find(bookId);

    book = shopDbContext
        .Books
        .FirstOrDefault(x => Math.Abs(x.PricePerUnit - 1373) < 0.001); // it will perform the query with top (1) returning the first element or the default value for Book class 
    book = shopDbContext
        .Books
        .SingleOrDefault(x => Math.Abs(x.PricePerUnit - 1373) < 0.001); //same as FirstOrDefault but the query have top(2) because this method throw an exception if the query returns multiple rows.
    if (book != null)
    {
        Console.WriteLine("BookId:{0} Name:{1} Price:{2}"
            , book.BookId, book.BookName, book.PricePerUnit);
    }
    else
    {
        Console.WriteLine("Book Not Found");
    }

    //select count(*) from Book
    var numberOfBooks = shopDbContext.Books.Count();
    Console.WriteLine("Number Of Books are " + numberOfBooks);

    //selece * from Book where PricePerUnit>5000
    var list2 = shopDbContext.Books.Where(x => x.PricePerUnit > 5000).ToList();
    //selece * from Book where AuthorId = 3
    var list3 = shopDbContext.Books.Where(x => x.AuthorId == 3).ToList();

    //Get All the books whose author's first name is "Manzoor"
    var authorId = shopDbContext
        .Authors
        .FirstOrDefault(x => x.FirstName == "Manzoor")?//? is the null propagation operator if firstordefault return null all expression will result null
        .AuthorId;
    //Select * from Book where AuthorId = authorId
    var list4 = shopDbContext.Books.Where(x => x.AuthorId == authorId).ToList();

    //Select * from Book join Author on Book.AuthorId = Author.Id where Author.FirstName = 'Manzoor'
    var list = shopDbContext.Books.Where(x => x.Author.FirstName == "Manzoor").ToList();

    foreach (var bookElement in list)
    {
        Console.WriteLine("BookId:{0} Name:{1} Price:{2}", bookElement.BookId, bookElement.BookName, bookElement.PricePerUnit);
    }

    Console.ReadLine();
}
```

# Immediate mode, Deferred mode
## Immediate mode
```c#
//Immediate Mode - List, IEnumerable
var list = context.Books.ToList();
```
The query is executed during the declaration.  
Try:
```c#
var list = context.Books.ToList();
list.Add(new Book() {BookName = "Prova", PricePerUnit = 5000, AuthorId = 1});
context.SaveChanges();
```
Do this insert the new book in the database ? (NO)
This will:
```c#
List<Book> list = context.Books.ToList();
context.Books.Add(new Book() {BookName = "Prova", PricePerUnit = 5000, AuthorId = 1});
context.SaveChanges();
```
Using the ToList() method it will **perform the query** but the new list is detached from the dbset: if we add or remove item is only for the list not for the context.
To restrict the user to add new item we can use IEnumerable() instead of List
```c#
IEnumerable<Book> list = context.Books.ToList();
```
IEnumerable is **read-only** List is not.
## Deferred mode
```c#
//Deffered Mode - IEnumerable, IQueryable
IEnumerable<Book> list = context.Books;
foreach (var book in list)
{
    ...
}
```
The query is prepared during the declaration and it is performed every time before use.
The variable, in this case, cannot be a List.
Try:
```c#
IEnumerable<Book> list = context.Books;
foreach (var item in list.Where(x => x.AuthorId != 1))
{
    ...
}
```
What query it will produce? It will do the same query as before so it load all records into memory then perform other operations.
If we use IQueryable instead of IEnumerable:
```c#
IQueryable<Book> list = context.Books;
foreach (var item in list.Where(x => x.AuthorId != 1))
{
    ...
}
```
It will performe the query with the where (AuthorId != 1) clause included.
### Summary
- **Immediate mode**: 
    * In immediate mode of query execution query gets executed whereverthe query is defined. It is achived with the help of linq method like ToList(), Count().
    * Query object holds the data wich reduces number of hits to database on every request.
    * Return type can be **List** or **IEnumerable**
- **Deferred mode**:
    * In deferred mode query is executed whenever we use it (whenever we acces the field)
    * Here query object hits the server on every request which reduces the size of query object in memory.
    * Return type can be **IQueryable** or **IEnumerable**
  

# Eager Loading, Explicit Loading, Lazy Loading
For the reverse list property like Books inside Author we can have different loading methods:  
**Eager loading:** load the collections specified with the _Include()_ method with the main query using joins  
```c#
var context = new ShopDbContext();
//Select * from Author a join Book b on a.Id = b.AuthorId
var authors = context.Authors.Include(x => x.Books).ToList();
foreach (var author in authors)
{
    foreach (var book in author.Books)
    {
        Console.WriteLine(book.BookId + " " + book.BookName)
    }
}
```
**Explicit loading:** load only the main entity (in this case Author) and when we need Author's collections we have to load them explicitly. 
```c#
var context = new ShopDbContext();
//Select * from Author
var authors = context.Authors.ToList();
foreach (var author in authors)
{
    //It will now hit SQL Server to execute query
    //Selecct * from Book where AuthorId = @CurrentForeachAuthor.Id
    context.Entry(author).Collection(x => x.Books).Load();
    foreach (var book in author.Books)
    {
        Console.WriteLine(book.BookId + " " + book.BookName)
    }
}
```
**Lazy loading:** same behavior as Explicit loading.  
To use lazy loading we need to add the Microsoft.EntityFrameworkCore.Proxies package to the project and the following command in the context OnConfiguring method
```c#
//using Microsoft.EntityFrameworkCore.Proxies
optionsBuilder.UseLazyLoadingProxies();
```
and then:
```c#
var context = new ShopDbContext();
var authors = context.Authors.ToList();
foreach (var author in authors)
{
    //LazyLoading: related employees of D gets loaded automatically
    foreach (var book in author.Books)
    {
        Console.WriteLine(book.BookId + " " + book.BookName)
    }
}
```
Using Lazy loading **ALL** navigation properties must be _virtual_

  
# Default transaction
Say we are inserting 1 record and updating 2 records and finally calling SaveChanges(); by default SaveChanges() are applied in a transaction which means if any of the operations fail then te entire transaction is rolled back.  
You can also manually control transaction if you have multiple SaveChages() method call.

<hr>
<hr>
<hr>
<hr>

# Esercizio finale
Lo scopo è creare una struttura che contenga la mappatura delle entità e le sue logiche fondamentali (CRUD, comportamenti specifici ecc...) che sia applicabile ad ogni progetto di interfacciamento con l'utente.
Dividere la soluzione in più progetti:
- Entities: contiene tutte le classi che servono a mappare il db
- Services: contiene tutte le logiche delle entità (CRUD)
- Interfaces: contiene le interfaccie dei servizi (tipo i file .h del C)

Lo scopo è costruie un sistema di interfacciamento con il db replicando una base dati per gestire le timbrature:
- Mappatura entità: 
  * Utente
  * Commessa
  * IntervalloTemporale
- CRUD: operazioni di crud sulle entità
- Calcolo ore giornaliere dati data inizio e data fine


### **Entities**
Non deve avere dipendenze e contiene la mappatura delle entità del database. 
- Aggiungere i pacchetti necessari.
- Classe base Entity di partenza**(Facoltativo)**.
- Una classe/file per ogni entità.
- Specificare i campi e relative relazioni utilizzando dataannotations

### **Services**
Azioni relative alle entità (Es. CRUD)
- Creazione del Contesto (Classe Repository)
- Aggiungere logging **(Facoltativo)**
- Aggiungere riferimento al progetto Entity
- Aggiungere DbSet nella classe repository per ogni Entità
- Collegamento e aggiornamento del database (nel caso in cui il progetto di partenza e il progetto con il repository sono diversi bisogna specificare due parametri nei comandi di migrazione: --Project e --StartupProject)  
    È inoltre necessario che lo startupproject referenzi il project
    ```
    add-migration -Project Services -StartupProject FinalEFConsole
    update-database -Project Services -StartupProject FinalEFConsole

    ```
- Crud di base partendo dalla classe Entity **(Si può generalizzare utilizzando parametri di tipo ma potrebbe essere troppo complicato quindi Facoltativa)**
  * Save (Insert e Update)
  * LoadAll
  * LoadSingle
  * Delete
  * LoadAndSelect 
- Servizio per ogni entitá

### **Interfaces** 
Contiene la descrizione dell'interfacciamento con i services
- Interfaccia per ogni servizio

### Parte console:
- Caricare un entità (differenza tra savechanges, aggiunta al contesto ecc ecc)
- Stampare un entità a video (lettura singola e lista)
- Stampare le ore giornaliere