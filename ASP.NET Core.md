# .Net Core vs .Net
![CoreVsNormal](img/CoreVsNormal.jpg)

# Struttura
![StrutturaCartelle](img/StrutturaCartelle.jpg)

The starting point of a .Net Core application is in the **Program.cs** file(**Main** method) that initializes and runs a Kestrel web server.
The **CreateWebHostBuilder** method uses the **Startup** class (defined in the Startup.cs file) to apply the configurations.
The **Startup** class has two methods in it:
- **ConfigureServices**: inside it we can add services to _IServiceCollection_ container it will make them available for _dependency injection_.
- **Configure**: it is a middleware, a pipeline to manage requests and responses.

# Getting Started with first static
- _File -> New Project -> Web -> ASP .NET Core Web Application -> Empty_  
- Add an html page in the wwwroot
- Edit configure method this way:
  * Before (we can se that it always write Hello World! on every request):
    ```c#        
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.Run(async (context) =>
            {
                //Responds to every requests with text "Hello World!"
                await context.Response.WriteAsync("Hello World!");
            });
        }
    ``` 
  * After (now we can call localhost/home.html): 
    ```c#
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseStaticFiles();
        }
    ```
  * Default page settings: (now we can call localhost/ and it will return home.html)    
    ```c#       
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            var defaultFileOptions = new DefaultFilesOptions
            {
                DefaultFileNames = new List<string>{ "home.html" }
            };
            app.UseDefaultFiles(defaultFileOptions);
            app.UseStaticFiles();
        }
    ```

# Getting Started with MVC Core Web App
- **Adding MVC Service**:
  * Change the ConfiguringServices:
    ```c#       
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
        }
    ```
- **Configuring route**:
  * Change the Configure:
    ```c#       
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseStaticFiles();
            app.UseMvc(routes => //Configure the route for MVC
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}"
                );
            });
        }
    ```  
    We are setting the path template as controller/action with default Home for controllers and Index for actions.
- **Controllers**: Every request hit a controller, so we need to create one. We should have all ours controllers in a folder called **Controllers**
  * Create a directory inside the project called **Controllers**
  * Right click on it -> Add -> Controller -> MVC Controller Empty (name: DepartmentController) (do it three times Department, Employee, Home); 
    It will create a class. This class inherit from Controller.
    A method inside controller class is called **Action**.
    Change the code:
    ```c#  
        //Controller
        public class DepartmentController : Controller
        {
            //Action
            public string GetDepartmentt()
            {
                return "Development";
            }
        }
        //-----------------------------------------------        
        public class HomeController : Controller
        {
            public string Index()
            {
                return "SBT - From Home Index";
            }
            public string GetDate()
            {
                return DateTime.Now.ToLongDateString();
            }
        }
        //----------------------------------------------- 
        public class EmployeeController : Controller
        {
            public string Index()
            {
                return "boborobo";
            }
        }
    ```
    Now if we start the application ad go to localhost.../Department/GetDepartment, the application will return "Development"  
    If we want to render html from string we can use:
    ```c#
        public ContentResult Index()
        {
            //return "boborobo";
            var html = "<h1> SBT - From home index </h1>";
            return Content(content: html, contentType: "text/html");
        }
    ```

## Summary
**M**odel - **V**iew - **C**ontroller is an architectural pattern wich separates an application's UI into three main blocks: model, view, controller.  
Any **Controller** is a class which is inherit from a base Controller class.  
User request is handled by a controller's method called **Action**

# Viewes
Whatever we see on the web page is a View.  
It is a presenting content file with .cshtml extension.  
It is send to the user in response to a request from controller.  
So: user launch the request -> controller receive it, look for a view and renders it -> controller send the response to the user.  

## Moving on to practice
- Add a view: right click on action name (Ex HomeController -> Index) -> Add View -> Add: It will create a Views folder with another folder (Home) with inside our view (Index.cshtml)
- Try with GetDate  

We need a folder for each controller that uses a view.  
Create a Department and Employee folder.  
Edit Index.cshtml   
```html
@{
    ViewData["Title"] = "Index";
}
<h1>Index</h1>
<div style="display: block">
    <div>
        <span>A penny for your thoughts: </span>
    </div>
    <div>
        <input type="text" />
    </div>
    <div>
        <input type="button" value="Save" />
    </div>
</div>
```
Now change the action to render view:
```c#
    public ViewResult Index()
    {
        return View("Index");
    }
```
If we does not specify the view name _return View();_ it will use the action name.
# LibMan
https://docs.microsoft.com/it-it/aspnet/core/client-side/libman/libman-vs?view=aspnetcore-2.2  
Library Manager (LibMan) is a lightweight, client-side library acquisition tool. LibMan downloads popular libraries and frameworks from the file system or from a content delivery network (CDN). The supported CDNs include CDNJS and unpkg. The selected library files are fetched and placed in the appropriate location within the ASP.NET Core project.  
Visual Studio has built-in support for LibMan in ASP.NET Core projects.  
Library files can be added to an ASP.NET Core project in two different ways:
- Use the Add Client-Side Library dialog: In Solution Explorer, right-click the project folder in which the files should be added. Choose Add > Client-Side Library
- Manually configure LibMan manifest file entries: Editing the libman.json file

Try to add Bootstrap and Jquery...
# NPM - Recommended
Alternative to LibMan is to use directly npm with usual commands (**recommended**):   
Initialization
    ```
    npm init 
    ```   
To install packages
    ```
    npm install packageName
    ```
# AppSettings File
Configuration information at application level like connectionstrings, etc.  
Right click on solution -> Add -> Add Item -> ASP.NET Configuration File.  
It will create the _appsettings.json_ file where we can store configuration like connectionstring.

# Get vs Post
Try parameter GET (passend in the URL) and POST (passed in body)
GET example:
- Throught URL on browser
- Specifying get method in form attribute
    ```html
    <form method="get" action="Home\Save">
        DepartmentId : <input type='text' id="i_DeptId" name="DeptId" /> <br />
        DepartmenttName : <input type="text" id="i_DeptName" name="DeptName" /><br />
        <input type='submit' value='Save' />
    </form>
    ```
- Using JS (```windows.location.href```, ```windows.open```)
- Ajax ecc...

POST example:
- Specifying get method in form attribute
    ```html
    <form method="post" action="Home\Save">
        DepartmentId : <input type='text' id="i_DeptId" name="DeptId" /> <br />
        DepartmenttName : <input type="text" id="i_DeptName" name="DeptName" /><br />
        <input type='submit' value='Save' />
    </form>
    ```
- Ajax ecc...
## Summary
- Get Method is used to request a resource from server
- In get method data is embedded in request URL
- Post method is used to send form data to the server
- In post method data is embedded in body form

# Passing data from view to controller
## Parameterized Method
Add parameter to the action with the same name as passed data. Is the same for get and post
```c#
     public ViewResult Save(int DeptId, string DeptName)
```
In this case DeptId and DeptName

## Context Object Method
We can access the data throught the Request object:
- For GET - **Request.Query**
    ```c#
    public ViewResult GetDept()
    {
        int DeptId = int.Parse(Request.Query["DeptId"].ToString());
        //Write Logic to read dept
        return View();
    }
    ``` 
- For POST - **Request.Form**
    ```c#
    public ViewResult Save()
    {
        int DeptId = int.Parse(Request.Form["DeptId"].ToString());
        string DeptName = Request.Form["DeptName"].ToString();
        //Write Logic to save
        return View();
    }
    ```
# Razor view engine
Razor is a new markup for mixing HTML and c#.  
**@** symbol is used to transalte c# to HTML Ex. html (escapable with @@)
```html
<p>Date is @DateTime.Now.ToString()</p>
```
Razor files have **.cshtml** extension.
## Statements logic
![RazorStatements](img/RazorStatements.jpg)  

# Helper
Helper class provide c# methods to render html.  
Form example:
```c#
@using (Html.BeginForm(
    actionName: "Save",
    controllerName: "Home",
    method: FormMethod.Post
))
{
    <text>
        DepartmentId : <input type='text' id="i_DeptId" name="DeptId" /> <br />
        DepartmenttName : <input type="text" id="i_DeptName" name="DeptName" /><br />
        <input type='submit' value='Save' />
    </text>
}
```
We have a lot of method already written like and generally we can divide they in two categories:
- The one who use Expression to get value and name (it is used a lot with model to bind data)
    ```c#
    @Html.LabelFor(model => model.Name, new { @class = "form-control"}) //REnder a label
    @Html.DropDownList(model => model.Employees, new List<SelectListItem>() {}, "", new //Render a select
    {
        @class = "form-control"

    })
    ```
- The one who take straight value and name:
    ```c#
    @Html.Label("name", "text", new { @class = "form-control"}) //REnder a label
    @Html.DropDownList("name", new List<SelectListItem>() {}, "", new //Render a select
    {
        @class = "form-control"

    })
    ```
Where htmlAttributes parameter will convert all the properties passed in tag attributes (in our case we use @ because class is a keyword in c#)
Try to build our form using only Html Helper:
```c#
@using (Html.BeginForm(
    actionName: "Save",
    controllerName: "Home",
    method: FormMethod.Post
))
{
    @Html.Label("UserName") <span>:</span> @Html.TextBox("UserName") </br>
    @Html.Label("Email") <span>:</span> @Html.TextBox("Email") </br>
    //Submit button:
    @Html.TextBox("Submit", "Submit", new { type = "Submit"})
    //or 
    <input type='submit' value='Save' /> //This can be also written with Html but is a best practice to write this way
}
```
NB: From the Toolbox window on visual studio we can drag and drop some prefab components.  
**Try the dropdownlist**:
- In the cshtml page add the following code to initialize and fill a selectList:
    ```c#
    @{
        var selectList = new SelectList(
            new List<SelectListItem>()
            {
                new SelectListItem("PrimoElemento", "1"),
                new SelectListItem("SecondoElemento", "2"),
                new SelectListItem("TerzoElemento", "3"),
                new SelectListItem("QuartoElemento", "4"),
            }, 
            "Text", 
            "Value"
        );
    }
    ```
    We can build a selecet list from all kind of IEnumerable, this constructor take three parameter: the list, the list object type property name for Text, the list object type property name for Value.
- Now we can use the new list to fill the DropDownList:
    ```c#
    @Html.DropDownList("Prova", selectList, "", new { @class = "dropdown"})
    ```
# ViewData vs ViewBag and EF in MVC
## State management or passing data
### Weakly typed
- View to controller
    * Parameterized method
    * Context object method
- Controller to view
    * ViewData (non-persistent):
        ViewData is a ```Dictionary<string, object>```.  
        We can add value to ViewData this way
        ```c#
        public IActionResult Index()
        {
            ViewData["MyData"] = 25;
            return View();
        }
        ```
        And now we can access it in the View
        ```c#
        @if (ViewData["MyData"] != null)
        {
            <p>@ViewData["MyData"].ToString()</p>
        }
        ```
        If we need to use "MyData" as integer it needs to be typed
        ```c#
        @if (ViewData["MyData"] != null)
        {
            <p>@(int.Parse(ViewData["MyData"].ToString()) + 10)</p>
        }
        ```
    * ViewBag (non-persistent)
        ViewBag is a dynamic object (The dynamic type enables the operations in which it occurs to bypass compile-time type checking).  
        We can add value to ViewBag this way:
        ```c#
        public IActionResult Index()
        {
            ViewBag.MyData = 25;
            return View();
        }
        ```
        And access data this way:
        ```c#
        <p>@ViewBag.MyData</p>
        ```
        It automatically check null and type, we also can use it without any cast or parse:
        ```c#
        <p>@(ViewBag.MyData + 5)</p>
        ```
        ViewBag is a wrap of ViewData, internally look ViewData but handle null data and type with the help of dynamic. So we can define value with ViewBag then read with ViewBag and vice versa.  
    **Summary**
    - **ViewData** is a dictionary of objects which stores data as key-value pair
    - Null values must be handled explicitly
    - We need to type cast ViewData to get it as the required type explicitly
    - **ViewBag** is also stores the data as key-value pair by using dynamic feature of c#.
    - Null values get handled implicitly
    - Type casting in ViewBag happens automatically

    **Exercise introducing EF**:
    - Add EF Core packages
    - Configure EF
    - Add **Data** folder and inside add Context class
    - Add **Models** folder and inside add entities classes
    - Build a Sudent entity: Name, Surname, Mail
    - Build a page with a form that take the StudentId and call the GetDetail(int studentId) action
    - Build the GetDetail(int studentId) action that redirect to a page with the student's details.
    - Build a page with the students grid
    - Add button to the grid that call GetDetail(thisRowStudentId) 
- Controller to controller **(Via View)**
    * TempData (non-persistent): it behaves like ViewData
    ```c#
    //Set
    TempData["Key"] = "Value";
    //Get
    var a = TempData["Key"].ToString();
    ```
- Everywhere
    * Session (persistent)
Data passed this way is not typed! So we don't have intelliSense and it needs to be typed
#### non-persistent: Once the controller finishes compiling the data will be deleted
### Strongly typed
**Model**: we can directly pass object to the view using a model:
```c#
public IActionResult ReadStudent(int studentId)
{
    var repository = new Repository();
    var student = repository.Employees.Find(studentId)
    return View(student);
}
```
To use it we have to bind the model to the view (inside the view) then we can access data with the keyword **Model**:
```c#
@model Project.Models.Student //this code bind the model to the view -> the model can be of any type
@{
    ViewData["Title"] = "ReadStudent";
}
<h2>Welcome to view - Index</h2>
<div>
    StudentId: @Model.StudentId </br>
    Name: @Model.Name </br>
    Mail: @Model.Mail </br>
</div>
```
**Exercise**: try to convert grid page with model
## Standard object creation in MVC - Adding a new Student Example
- Add a Create() action to get the form
- Add another Create() action to post the form
- Use @Html._Control_For() to work with model bindings
- Use ActionLink() for links to call action
- Use RedirectToAction() to move from one action to another action
- Finally we will see using **TempData** to pass a message from one action to another action

To do:
- Add a specific controller for the entity
- Index view (with relative Index get method) -> grid or list of all Studen entity and an action link "Add new student" to the create get method.  
    View:
    ```c#
    @model List<ViewDataVsViewBagEg.Models.Student>

    @{
        ViewData["Title"] = "Index";
    }

    <h2>Index</h2>

    @Html.ActionLink("Add New Student","Create","Students")

    <ul>
        @foreach (var student in Model)
        {
            <li>@student.Name  @student.Email</li>
        }
    </ul>
    ```
    Action: 
    ```c#
    public IActionResult Index()
    {
        SchoolDbContext schoolDbContext = new SchoolDbContext();
        List<Student> students = schoolDbContext.Students.ToList();
        return View(students);
    }
    ```
- Create view (with relative Create get method)-> add a form and bind the model
    View:
    ```c#
    @model ViewDataVsViewBagEg.Models.Student
    @{
        ViewData["Title"] = "Create";
    }

    @using (Html.BeginForm("Create", "Students", FormMethod.Post))
    {
    <div>
        @Html.LabelFor(x => x.Name) : @Html.TextBoxFor(x => x.Name) //@Html._Control_For() to bind model
        @Html.LabelFor(x => x.Email) : @Html.TextBoxFor(x => x.Email)
        <input type="submit" value="Create"/>
    </div>
    }
    ```
    Action:
    ```c#
    public IActionResult Create()
    {
        return View();
    }
    ```
- Create post action:
    ```c#
    public IActionResult Create(Student S)
    {
        SchoolDbContext schoolDbContext = new SchoolDbContext();
        schoolDbContext.Add<Student>(S);
        schoolDbContext.SaveChanges();

        return RedirectToAction("Create", "Students");
    }
    ```
Probably this way will generate an error because in the same controller there are two Create action, we need to specify which one is for Post method and which one is for Get method with DataAnnotations: ```[HttpPost]``` ```[HttpGet]```, above the relevant method.
- Add a Try Catch to Create post action with error messages
    ```c#
    [HttpPost]
    public IActionResult Create(Student S)
    {
        //string Msg;
        try
        {
            SchoolDbContext schoolDbContext = new SchoolDbContext();
            schoolDbContext.Add<Student>(S);
            schoolDbContext.SaveChanges();
            TempData["Message"] = "Success!";
        }
        catch (Exception E)
        {
            TempData["Message"] = "Failed!";
        }

        return RedirectToAction("Create", "Students");
    }
    ```
    TempData is the non-presistent wekly typed way to pass dato from an action to another action.
    We need also to display the message inside the view:
    ```c#
    <h3>
        @if(TempData["Msg"]!=null)
        {
            <p>@TempData["Msg"].ToString()</p>
        }
    </h3>
    ```