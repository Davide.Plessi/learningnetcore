# Installation
- Pull the SQL Server 2017 Linux container
    ```bash
    docker pull mcr.microsoft.com/mssql/server:2017-latest
    ```
- Run the container image
    ```bash
    docker run -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=<YourStrong!Passw0rd>' \
        -p 1433:1433 --name mssql \
        -d mcr.microsoft.com/mssql/server:2017-latest
    ```
# Start/Stop
- To start the container
    ```bash
    docker start mssql
    ```
- To stop the container
    ```bash
    docker stop mssql
    ```

# Connection string
Find the container ip:
  * Connect to the container bash:``` docker exec -t mssql bash ```
  * Use use ``` ifconfig ``` or ``` ip addr ```

Then the connection string will be:  
"Data Source=<``` container ip ```>,1433;Initial Catalog=<``` db instance name ```>;Persist Security Info=True;User ID=SA;Password=<``` YourStrong!Passw0rd ```>"