﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities
{
    [Table("Utenti")]
    public class Utente : Entity
    {
        [Column("Nome")]
        [MaxLength(50)]
        [Required]
        public string Nome { get; set; }

        [Column("Cognome")]
        [MaxLength(50)]
        [Required]
        public string Cognome { get; set; }

        [Column("Matricola")]
        [MaxLength(10)]
        public string Matricola { get; set; }

        public virtual List<PeriodoTemporale> PeriodiTemporali { get; set; }        
    }
}
