﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities
{
    [Table("Commesse")]
    public class Commessa : Entity
    {
        [Column("Nome")]
        [MaxLength(20)]
        [Required]
        public string Nome { get; set; }

        public virtual List<PeriodoTemporale> PeriodiTemporali { get; set; }
    }
}
