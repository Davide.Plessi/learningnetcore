﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities
{
    [Table("PeriodiTemporali")]
    public class PeriodoTemporale : Entity
    {
        [Column("DataInizio")]
        [Required]
        public DateTime DataInizio { get; set; }

        [Column("DataFine")]
        public DateTime? DataFine { get; set; }

        [Column("Note")]
        public DateTime Note { get; set; }

        [Column("CommessaId")]
        [ForeignKey("Commessa")]
        [Required]
        public int CommessaId { get; set; }
        public Commessa Commessa { get; set; }

        [Column("UtenteId")]
        [ForeignKey("Utente")]
        [Required]
        public int UtenteId { get; set; }
        public Utente Utente { get; set; }

        [NotMapped]
        public bool IsValid => DataFine.HasValue && DataFine.Value > DataInizio;
    }
}
