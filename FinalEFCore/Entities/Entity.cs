﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities
{
    abstract public class Entity
    {
        [Key]
        [Column("Id")]
        public int Id { get; set; }
    }
}
