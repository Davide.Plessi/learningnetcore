﻿using Entities;
using Interfaces;

namespace Services
{
    public class PeriodoTemporaleService : CrudService<PeriodoTemporale, Repository>, IPeriodoTemporaleService
    {

        public PeriodoTemporaleService
            ( Repository dbContext
            ) : base(dbContext)
        {
        }
    }
}
