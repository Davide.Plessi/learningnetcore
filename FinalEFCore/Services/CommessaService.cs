﻿using Entities;
using Interfaces;

namespace Services
{
    public class CommessaService : CrudService<Commessa, Repository>, ICommessaService
    {
        public CommessaService
            ( Repository dbContext
            ) : base(dbContext)
        {
        }
    }
}
