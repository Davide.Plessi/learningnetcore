﻿using Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Services
{
    public class Repository : DbContext
    {
        private const string ConnectionString = "Data Source=localhost\\SQLEXPRESS2017;" +
            "Initial Catalog=CorsoMvc;Persist Security Info=True;" +
            "User ID=sa;Password=SystemAdmin2017;";

        public DbSet<Commessa> Commesse { get; set; }
        public DbSet<PeriodoTemporale> PeriodiTemporali { get; set; }
        public DbSet<Utente> Utenti { get; set; }

        private static ILoggerFactory GetLoggerFactory()
        {
            IServiceCollection serviceCollection = new ServiceCollection();
            serviceCollection.AddLogging(builder =>
                builder
                    .AddDebug()
                    .AddFilter(DbLoggerCategory.Database.Command.Name, LogLevel.Information));
            return serviceCollection.BuildServiceProvider().GetService<ILoggerFactory>();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseLoggerFactory(GetLoggerFactory())
                .UseSqlServer(ConnectionString);
        }
    }
}
