﻿using Entities;
using Interfaces;

namespace Services
{
    public class UtenteService : CrudService<Utente, Repository>, IUtenteService
    {
        public UtenteService
            ( Repository dbContext
            ) : base(dbContext)
        {
        }
    }
}
