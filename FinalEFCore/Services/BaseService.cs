﻿using Microsoft.EntityFrameworkCore;

namespace Services
{
    public abstract class BaseService<TDbContext>
        where TDbContext : DbContext
    {
        protected TDbContext DbContext { get; }

        protected BaseService(TDbContext dbContext)
        {
            DbContext = dbContext;
        }
    }
}
