﻿using Entities;

namespace Interfaces
{
    public interface IPeriodoTemporaleService : ICrudService<PeriodoTemporale>
    {
    }
}
