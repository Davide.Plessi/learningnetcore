﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Interfaces
{
    public interface ICrudService<T>
        where T : Entity, new()
    {
        List<T> LoadAll();

        T LoadSingle(int id);

        int Delete(int id);

        int Save(T entity);

        List<TResult> LoadAndSelect<TResult>(
            Expression<Func<T, bool>> filter,
            Expression<Func<T, TResult>> selector);
    }
}
