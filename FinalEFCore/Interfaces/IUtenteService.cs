﻿using Entities;

namespace Interfaces
{
    public interface IUtenteService : ICrudService<Utente>
    {
    }
}
