﻿using Entities;

namespace Interfaces
{
    public interface ICommessaService : ICrudService<Commessa>
    {
    }
}
