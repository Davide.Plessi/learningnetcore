﻿
using Entities;
using Services;
using System;
using System.Collections.Generic;

namespace FinalEFCore
{
    class Program
    {
        public static IEnumerable<Utente> NextUtente
        {
            get
            {
                yield return new Utente { Nome = "Stefano", Cognome = "Bellotti", Matricola = "1" };
                yield return new Utente { Nome = "Davide", Cognome = "Plessi", Matricola = "2" };
                yield return new Utente { Nome = "Emma", Cognome = "Coradduzza", Matricola = "3" };
                yield return new Utente { Nome = "Sandro", Cognome = "Sandrolini", Matricola = "4" };
            }
        }
        public static IEnumerable<Commessa> NextCommessa
        {
            get
            {
                yield return new Commessa { Nome = "Gp2" };
                yield return new Commessa { Nome = "Formazione" };
            }
        }

        static void Main(string[] args)
        {
            var repository = new Repository();

            var commessaService = new CommessaService(repository);
            var periodoTemporaleService = new PeriodoTemporaleService(repository);
            var utenteService = new UtenteService(repository);

            #region INSERT SOME DATA - Run Once!
            //foreach (var utente in NextUtente)            
            //    utenteService.Save(utente);

            //foreach (var commessa in NextCommessa)
            //    commessaService.Save(commessa); 
            #endregion

            foreach (var utente in utenteService
                .LoadAndSelect(
                    x => true,
                    x => new { x.Cognome, x.Nome, x.Matricola}))
            {
                Console.WriteLine(utente.Cognome + " " + utente.Nome + ", " + utente.Matricola);
            }
        }
    }
}
