# Redesigning a form with Tag Helper
Tag helpers participate in creating and rendering HTML elements in Razor files which provides an HTML-friendly development expirience.  
To use it we need to add ```@addTagHelper *, Microsoft.AspNetCore.Mvc.TagHelpers``` reference at the top of cshtml file.  
Form example:
```c#
    @using (Html.BeginForm("Create", "Department", FormMethod.Post))
    {
        <div class="form-group">
            @Html.LabelFor(model => model.Name)
            @Html.TextBoxFor(model => model.Name)
        </div>
        <div class="form-group">
            @Html.LabelFor(model => model.Description)
            @Html.TextBoxFor(model => model.Description)
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Add"/>
        </div>
    }
```
Can be converted to:
```html
    <form asp-controller="Department" asp-action="Create" method="post">
        <div class="form-group">
            <label asp-for="Name"></label>
            <input asp-for="Name" class="form-control" type="text">
        </div>
        <div class="form-group">
            <label asp-for="Description"></label>
            <input asp-for="Description" class="form-control" type="text">
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-dark" value="Add">
        </div>
    </form>
```
https://docs.microsoft.com/it-it/aspnet/core/mvc/views/tag-helpers/intro?view=aspnetcore-2.2

# Server side form validations
We need to specify with the help of DataAnnotations every property validation on the model:
```c#
    [Table("Departments")]
    public class Department
    {
        [Key]
        [Column("Id")]
        public int Id { get; set; }

        [Column("Name")]
        [Required(ErrorMessage = "Name is required!")] //Validation + error message
        [MaxLength(20, ErrorMessage = "Maximum length of 20 characters exceeded!")]
        public string Name { get; set; }

        [Column("Description")]
        [MaxLength(50, ErrorMessage = "Maximum length of 50 characters exceeded!")]
        [DefaultValue("No description")]
        public string Description { get; set; }

        public virtual List<Employee> Employees { get; set; }

        public virtual List<CustomerDepartment> CustomerDepartments { get; set; }
    }
```
Then we need to add the validations span:
```html
        
        @using (Html.BeginForm("Create", "Department", FormMethod.Post))
        {
            <div class="form-group">
                @Html.LabelFor(model => model.Name)
                @Html.TextBoxFor(model => model.Name)
                @Html.ValidationMessageFor(model => model.Name)
            </div>
            <div class="form-group">
                @Html.LabelFor(model => model.Description)
                @Html.TextBoxFor(model => model.Description)
                @Html.ValidationMessageFor(model => model.Description)
            </div>
            @Html.ValidationSummary()
            <div class="form-group">
                <input type="submit" class="btn btn-dark" value="Add" />
            </div>
        }

        <form asp-controller="Department" asp-action="Create" method="post">
            <div class="form-group">
                <label asp-for="Name"></label>
                <input asp-for="Name" class="form-control" type="text">
                <span asp-validation-for="Name" class="error"></span>
            </div>
            <div class="form-group">
                <label asp-for="Description"></label>
                <input asp-for="Description" class="form-control" type="text">
                <span asp-validation-for="Description" class="error"></span>
            </div>
            <div class="form-group error" asp-validation-summary="All"></div>
            <div class="form-group">
                <input type="submit" class="btn btn-dark" value="Add">
            </div>
        </form>
```
And inside the Controller action Create (POST):
```c#
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Create(Department entity)
    {
        try
        {
            if (!ModelState.IsValid) return View();
            using (var repo = new Repository())
            {
                repo.Add(entity);
                repo.SaveChanges();
            }
            TempData["Message"] = "Success!";
            return RedirectToAction(nameof(Index));
        }
        catch (Exception exc)
        {
            TempData["Message"] = exc.Message;
            return View();
        }
    }
```
Other validation annotations:
```c#
        //Compare the ConfirmPassword property with Passord
        public string Password { get; set; }
        [Compare("Password")]
        public string ConfirmPassord { get; set; }

        //Rating can be a value between 0 and 100  
        [Range(0, 100)]
        public int Rating { get; set; }

        //Check if the inserted string is a valid email
        [EmailAddress]
        public string email { get; set; }

        //heck if the inserted string is a valid URL
        [Url]
        public string ProfileUrl { get; set; }

```
https://docs.microsoft.com/it-it/aspnet/core/mvc/models/validation?view=aspnetcore-2.2#validation-attributes
## Custom validation
We need to create new Attribute that inherit from ValidationAttribute and override the mehon IsValid().
For example we want to prohibit certain values:  
First of all we need to add the Attribute class
```c#
    public class RestrictedValue : ValidationAttribute
    {
        public List<string> ForbiddenValues { get; set; }
        public RestrictedValue(string[] forbiddenValues, string errorMessage = null) : base()
        {
            ForbiddenValues = forbiddenValues?.ToList() ?? new List<string>();
            ErrorMessage = errorMessage ?? "Forbidden value";
        }
        protected override ValidationResult IsValid(
            object value, 
            ValidationContext validationContext
        )
        {
            return ForbiddenValues.Contains(value) 
                ? new ValidationResult(ErrorMessage) 
                : ValidationResult.Success;
        }
    }
```
Then we can put it on the model property
```c#
    [Table("Departments")]
    public class Department
    {
        [Key]
        [Column("Id")]
        public int Id { get; set; }

        [Column("Name")]
        [RestrictedValue(new []{ "Prodotti" })]
        [Required(ErrorMessage = "Name is required!")]
        [MaxLength(20, ErrorMessage = "Maximum length of 20 characters exceeded!")]
        public string Name { get; set; }

        [Column("Description")]
        [MaxLength(50, ErrorMessage = "Maximum length of 50 characters exceeded!")]
        [DefaultValue("No description")]
        public string Description { get; set; }

        public virtual List<Employee> Employees { get; set; }

        public virtual List<CustomerDepartment> CustomerDepartments { get; set; }
    }
```

# Client side form validations
We need to add three client-side libraries:
- jquery
- jquery-validation
- jquery-validation-unobtrusive
Then include them in the cshtml page.

# Complete the CRUD operations
## Index
```html
    @model List<Department>
    @{
        ViewData["Title"] = "Index";
    }
    @Html.ActionLink(
        "Create new department", 
        "Create", 
        "Department", 
        null, 
        new { @class = "btn btn-dark" }) 
    <br/>
    <br/>
    <br/>
    <table class="table">
        <thead class="table-dark">
        <tr>
            <th scope="col">Id</th>
            <th scope="col">Name</th>
            <th scope="col">Description</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        @foreach (var dep in Model)
        {
            <tr>
                <td id="dep-id">@dep.Id</td>
                <td>@dep.Name</td>
                <td>@dep.Description</td>
                <td>
                    <a asp-action="Edit" asp-controller="Department" asp-route-id="@dep.Id">
                        Edit
                    </a>
                    <a asp-action="Delete" asp-controller="Department" asp-route-id="@dep.Id">
                        Delete
                    </a>
                </td>
            </tr>
        }
        </tbody>
    </table>
    @section Scripts
    {
        <script>
            var trs = document.querySelectorAll("tbody > tr");
            if (trs && trs.length > 0) {
                trs.forEach((value, index) => {
                    value.classList.add("cursor-pointer");
                    value.addEventListener("click",
                        function() {
                            const elementId = parseInt(this.childNodes[1].textContent.trim());
                            window.location.href = `/Department/Details/${elementId}`;
                        });
                });
            }
        </script>
    }
```
## Edit
```html
    @model ThirdASPNETCore.Models.Department

    @{
        ViewData["Title"] = "Edit";
    }

    <h1>Edit</h1>

    <h4>Department</h4>
    <hr />
    <div class="row">
        <div class="col-md-4">
            <form asp-action="Edit">
                <input type="hidden" asp-for="Id" />
                <div class="form-group">
                    <label asp-for="Name" class="control-label"></label>
                    <input asp-for="Name" class="form-control" />
                    <span asp-validation-for="Name" class="text-danger"></span>
                </div>
                <div class="form-group">
                    <label asp-for="Description" class="control-label"></label>
                    <input asp-for="Description" class="form-control" />
                    <span asp-validation-for="Description" class="text-danger"></span>
                </div>
                <div asp-validation-summary="ModelOnly" class="text-danger"></div>
                <div class="form-group">
                    <input type="submit" value="Save" class="btn btn-primary" />
                </div>
            </form>
        </div>
    </div>

    <div>
        <a asp-action="Index">Back to List</a>
    </div>
```
```c#
    // GET: Department/Edit/5
    [HttpGet]
    public ActionResult Edit(int id)
    {
        var repo = new Repository();
        var entity = repo.Set<Department>().Find(id);
        repo.Dispose();
        return View(entity);
    }

    // POST: Department/Edit
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Edit(Department entity)
    {
        try
        {
            if (!ModelState.IsValid) return View();
            using (var repo = new Repository())
            {
                repo.Update(entity);
                repo.SaveChanges();
            }
            return RedirectToAction(nameof(Index));
        }
        catch (Exception exc)
        {
            TempData["Exception"] = exc.Message;
            return View();
        }
    }
```
## Delete
```c#
    public ActionResult Delete(int id)
    {
        try
        {
            using (var repo = new Repository())
            {
                repo.Remove(new Department {Id = id});
                repo.SaveChanges();
            }
        }
        catch (Exception exc)
        {
            TempData["Exception"] = exc.Message;
        }
        return RedirectToAction(nameof(Index));

    }
```

# Dependency Injection
Instead of create in each method the repository object we can create a field then initialize it in the Controller constructor:
```c#
public class DepartmentController : Controller
    {
        private readonly Repository _repository;

        public DepartmentController()
        {
            _repository = new Repository();
        }
        //...
    }
```
And then use _repository :
```c#
    //...
    
    // GET: Department
    public ActionResult Index()
    {
        var entities = _repository.Set<Department>().ToList();
        return View(entities);
    }

    //...

    // POST: Department/Create
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Create(Department entity)
    {
        try
        {
            if (!ModelState.IsValid) return View();
            _repository.Add(entity);
            _repository.SaveChanges();
            return RedirectToAction(nameof(Index));
        }
        catch (Exception exc)
        {
            TempData["Exception"] = exc.Message;
            return View();
        }
    }

    //...
```
The controller is initializet at every request so too the _repository field. But we are not disposing it so we need to override the dispose method of contrellr that will be executed automatically from the garbage collector.
```c#
    //...

    protected override void Dispose(bool disposing)
    {
        if (disposing)
        {
            _repository.Dispose();
        }
        base.Dispose(disposing);
    }

    //...
```
But now the Controller constructor depends on Repository constructor (if we need a parameter to initialize Repository we need to add the same parameter inside the controller constructor), this also mean that every controller create an object.  
To avoid this situation we can use **Dependency Injection**:  
1. Edit the Controller constructor
    ```c#
        //...

        private readonly Repository _repository;

        public DepartmentController(Repository repository)
        {
            _repository = repository;
        }

        //...
    ```
2. Edit the ConfigureService method inside Startup.cs
    ```c#
        //...

        public void ConfigureServices(IServiceCollection services)
        {
            //this row inject the repository object inside the controller constructor
            services.AddDbContext<Repository>();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        //...
    ```
Now we can also delete the Dispose override because we don't need to dispose the new object.


# Layout
Layout is a common page that will be the same for all our views.  
To add a layout:
1. Add a cshtm file named _Layout under /Views/Shared like this
    ```html
    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>@ViewData["Title"] - ThirdASPNETCore</title>

        <environment include="Development">
            <link rel="stylesheet" href="~/lib/bootstrap/dist/css/bootstrap.css" />
        </environment>
        <environment exclude="Development">
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
                asp-fallback-href="~/lib/bootstrap/dist/css/bootstrap.min.css"
                asp-fallback-test-class="sr-only" asp-fallback-test-property="position" asp-fallback-test-value="absolute"
                crossorigin="anonymous"
                integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"/>
        </environment>
        <link rel="stylesheet" href="~/css/site.css" />
    </head>
    <body>
        <header>
            <nav class="navbar navbar-expand-sm navbar-toggleable-sm navbar-light bg-white border-bottom box-shadow mb-3">
                <div class="container">
                    <a class="navbar-brand" asp-area="" asp-controller="Home" asp-action="Index">ThirdASPNETCore</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".navbar-collapse" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="navbar-collapse collapse d-sm-inline-flex flex-sm-row-reverse">
                        <ul class="navbar-nav flex-grow-1">
                            <li class="nav-item">
                                <a class="nav-link text-dark" asp-area="" asp-controller="Home" asp-action="Index">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-dark" asp-area="" asp-controller="Department" asp-action="Index">Department</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
    <div class="container">
        <main role="main" class="pb-3">
            @if (TempData["Exception"] != null)
            {
                <div class="exception">
                    <span >@TempData["Exception"]</span>
                </div>
            }
            @RenderBody()
        </main>
    </div>

        <footer class="border-top footer text-muted">
            <div class="container">
                &copy; 2019 - ThirdASPNETCore
            </div>
        </footer>

        <environment include="Development">
            <script src="~/lib/jquery/dist/jquery.js"></script>
            <script src="~/lib/bootstrap/dist/js/bootstrap.bundle.js"></script>
        </environment>
        <environment exclude="Development">
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"
                    asp-fallback-src="~/lib/jquery/dist/jquery.min.js"
                    asp-fallback-test="window.jQuery"
                    crossorigin="anonymous"
                    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=">
            </script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"
                    asp-fallback-src="~/lib/bootstrap/dist/js/bootstrap.bundle.min.js"
                    asp-fallback-test="window.jQuery && window.jQuery.fn && window.jQuery.fn.modal"
                    crossorigin="anonymous"
                    integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o">
            </script>
        </environment>
    <script src="~/js/site.js" asp-append-version="true"></script>
    <partial name="_ValidationScriptsPartial"/>

        @RenderSection("Scripts", required: false)
    </body>
    </html>
    ```
    The other view will be rendered where we call @RenderBody().  
    The environment tag use the environment variable inside the launchSettings.json to render different code.  
    @RenderSection render the code of the section specified (for example we can write code inside the @section Scripts {} inside the view and it will be renderend where @RenderSection("Scripts", required: false) is specified).  
    The partial tag render a partial view.  
2. Now we need to specify the _Layout.cshtml file as the Layout page:
    ```html
    @{
        Layout = "_Layout"; 
    }
    ```
    We can specify it on every .cshtml file or we can put inside the _ViewStart.cshtml.

## _ViewImprts.cshtml
This file is used to store the generic using directives inside one file. We need to add it in /Views.
```html
@using ThirdASPNETCore
@using ThirdASPNETCore.Models
@addTagHelper *, Microsoft.AspNetCore.Mvc.TagHelpers

```

# Areas
Right on the project -> Add -> Area
Area is a module that contain Models - Views - Data - Controllers.
We need to add the MVC pattern inside Startup.cs this way:
```c#
app.UseMvc(routes =>
{
    routes.MapRoute(
        name: "default",
        template: "{controller=Home}/{action=Index}/{id?}");
    routes.MapRoute(
        name: "areas",
        template: "{area:exists}/{controller=Home}/{action=Index}/{id?}"
    );
});
```
We also need inside the controller to specify which area it belongs to
```c#
[Area("Name")]
public ControllerName : Controller
{
 //...    
}
```

# Authentication
To add authentication we need to :
1. Add a new package: ```Microsoft.AspNetCore.Identity.EntityFrameworkCore```
2. Change the Repository to inherit from IdentityDbContext
    ```c#
    using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using ThirdASPNETCore.Models;

    namespace ThirdASPNETCore.Data
    {
        public class Repository : IdentityDbContext
        {
            private const string ConnectionString = "Data Source=10.0.75.1,1433;" +
                "Initial Catalog=ThirdASPNETCore;Persist Security Info=True;" +
                "User ID=sa;Password=sa2016!NSI;";

            public DbSet<Department> Departments { get; set; }
            public DbSet<Employee> Employees { get; set; }
            public DbSet<Customer> Customers { get; set; }
            public DbSet<CustomerDepartment> CustomerDepartments { get; set; }

            private static ILoggerFactory GetLoggerFactory()
            {
                IServiceCollection serviceCollection = new ServiceCollection();
                serviceCollection.AddLogging(builder =>
                    builder
                        .AddDebug()
                        .AddFilter(DbLoggerCategory.Database.Command.Name, LogLevel.Information));
                return serviceCollection.BuildServiceProvider().GetService<ILoggerFactory>();
            }

            protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            {
                optionsBuilder
                    .UseLoggerFactory(GetLoggerFactory())
                    .UseSqlServer(ConnectionString);
            }
        }
    }
    ```
3. Add the service to ConfigureService method:
    ```c#    
    public void ConfigureServices(IServiceCollection services)
    {
        services.Configure<CookiePolicyOptions>(options =>
        {
            options.CheckConsentNeeded = context => true;
            options.MinimumSameSitePolicy = SameSiteMode.None;
        });
        services.AddDbContext<Repository>();
        //Identity
        services
            .AddIdentity<IdentityUser, IdentityRole>()
            .AddEntityFrameworkStores<Repository>();
        services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
    }
    ```
4. Add authentication usage to Configure method:
    ```c#
    public void Configure(IApplicationBuilder app, IHostingEnvironment env)
    {
        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
        }
        else
        {
            app.UseExceptionHandler("/Home/Error");
            app.UseHsts();
        }

        app.UseHttpsRedirection();
        app.UseStaticFiles();
        app.UseCookiePolicy();
        app.UseAuthentication();

        app.UseMvc(routes =>
        {
            routes.MapRoute(
                name: "default",
                template: "{controller=Home}/{action=Index}/{id?}");
        });
    }
    ```
5. Add the Account controller and the views to manage users and log in
   1. Inject the user manager
    ```c#
    private UserManager<IdentityUser> _userManager;

    public AccountController(UserManager<IdentityUser> userManager)
    {
        _userManager = userManager;
    }
    ```
   2. Registration:
      - Model (RegisterViewModel.cshtml)
        ```c#
        using System.ComponentModel.DataAnnotations;

        namespace ThirdASPNETCore.Models
        {
            public class RegisterViewModel
            {
                [Required]
                public string UserName { get; set; }

                [Required]
                [EmailAddress]
                public string Email { get; set; }

                [Required]
                [RegularExpression("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[^\\da-zA-Z]).{8,15}$")]
                public string Password { get; set; }

                [Required]
                [Compare("Password")]
                public string ConfirmPassword { get; set; }
            }
        }
        ``` 
      - View 
        ```html
        @model RegisterViewModel
        @{
            ViewData["Title"] = "Sign up";
        }
        <div class="row">
            <div class="col-md-4">
                <form asp-controller="Account" asp-action="Register" method="post">
                    <div class="form-group">
                        <label asp-for="UserName"></label>
                        <input asp-for="UserName" class="form-control" type="text">
                        <span asp-validation-for="UserName"></span>
                    </div>
                    <div class="form-group">
                        <label asp-for="Email"></label>
                        <input asp-for="Email" class="form-control" type="email">
                        <span asp-validation-for="Email" class="error"></span>
                    </div>
                    <div class="form-group">
                        <label asp-for="Password"></label>
                        <input asp-for="Password" class="form-control" type="password">
                        <span asp-validation-for="Password" class="error"></span>
                    </div>
                    <div class="form-group">
                        <label asp-for="ConfirmPassword"></label>
                        <input asp-for="ConfirmPassword" class="form-control" type="password">
                        <span asp-validation-for="ConfirmPassword" class="error"></span>
                    </div>
                    <div class="form-group error" asp-validation-summary="All"></div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-dark" value="Sign-Up">
                    </div>
                </form>
            </div>
        </div>
        ```
      - Controller
        ```c#
        [HttpGet]
        public ActionResult Register()
        {
            return View(new RegisterViewModel());
        }

        [HttpPost]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            var user = new IdentityUser
            {
                UserName = model.UserName,
                Email = model.Email
            };
            var result = await _userManager.CreateAsync(user, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction(nameof(Login));
            }

            return View();
        }

        ```
   3. Inject the sign in manager:
        ```c#
        private readonly UserManager<IdentityUser> _userManager;
        private readonly SignInManager<IdentityUser> _signInManager;

        public AccountController(
            UserManager<IdentityUser> userManager,
            SignInManager<IdentityUser> sigInManager
        )
        {
            _userManager = userManager;
            _signInManager = sigInManager;
        }
        ``` 
   4. Log in:
      - Model (LogInViewModel.cs):
        ```c#
        using System.ComponentModel.DataAnnotations;

        namespace ThirdASPNETCore.Models
        {
            public class LogInViewModel
            {
                [Display(Name = "User name")]
                [Required]
                public string UserName { get; set; }

                [Required]
                public string Password { get; set; }

                [Display(Name = "Remember me")]
                public bool RememberMe { get; set; }

                public string ReturnUrl { get; set; }

                public LogInViewModel() { }

                public LogInViewModel(string returnUrl)
                {
                    ReturnUrl = returnUrl;
                }
            }
        }
        ```
      - View:
        ```html
        @model LogInViewModel
        @{
            ViewData["Title"] = "Log in";
        }
        <div class="row">
            <div class="col-md-4">
                <form asp-controller="Account" asp-action="Login" method="post">
                    <div class="form-group">
                        <label asp-for="UserName"></label>
                        <input asp-for="UserName" class="form-control" type="text">
                    </div>
                    <div class="form-group">
                        <label asp-for="Password"></label>
                        <input asp-for="Password" class="form-control" type="password">
                    </div>
                    <div class="form-group">
                        <label asp-for="RememberMe"></label>
                        <input asp-for="RememberMe" type="checkbox">
                    </div>
                    <input asp-for="ReturnUrl" type="hidden"/>
                    <input type="submit" value="Log in" class="btn btn-dark"/>
                </form>
            </div>
        </div>
        ```
      - Controller
        ```c#
        [HttpGet]
        public ActionResult Login()
        {
            return View(new LogInViewModel(Request.Query["ReturnUrl"]));
        }

        [HttpPost]
        public async Task<ActionResult> Login(LogInViewModel model)
        {
            var result = await _signInManager
                .PasswordSignInAsync(
                    model.UserName, 
                    model.Password, 
                    model.RememberMe, 
                    false
                );
            if (result.Succeeded)
                return Redirect(
                    string.IsNullOrEmpty(model.ReturnUrl)
                        ? Url.Action("Index", "Home")
                        : model.ReturnUrl
                );

            return View();
        }
        ```
    5. Log out, we only need an action on the AccountController:
        ```c#
        public async Task<ActionResult> Logout()
        {
            if(User.Identity.IsAuthenticated)
                await _signInManager.SignOutAsync();
            return RedirectToAction("Login", "Account");
        }
        ```
## Restrict access to specific controller
We can add ```[Autorize]``` to restrict access to unauthenticated users.
```c#
//...

[Authorize]
public class DepartmentController : Controller
{
    //...
```
## User object inside views
Inside views we can access User object to verify if it's authenticated and get other information about it:
```html
<header>
    <nav class="navbar navbar-expand-sm navbar-toggleable-sm navbar-light bg-white border-bottom box-shadow mb-3">
        <div class="container">
            <a class="navbar-brand" asp-area="" asp-controller="Home" asp-action="Index">
                ThirdASPNETCore
                @if (User.Identity.IsAuthenticated)
                {
                    <span>- Hi @User.Identity.Name!</span>
                }
            </a>
            
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".navbar-collapse" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="navbar-collapse collapse d-sm-inline-flex flex-sm-row-reverse">
                <ul class="navbar-nav flex-grow-1">
                    <li class="nav-item">
                        <a class="nav-link text-dark" asp-area="" asp-controller="Home" asp-action="Index">Home</a>
                    </li>
                    @if (!User.Identity.IsAuthenticated)
                    {
                        <li class="nav-item">
                            <a class="nav-link text-dark" asp-area="" asp-controller="Account" asp-action="Login">Log in</a>
                        </li>
                    }
                    else
                    {
                        <li class="nav-item">
                            <a class="nav-link text-dark" asp-area="" asp-controller="Account" asp-action="Logout">Log out</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-dark" asp-area="" asp-controller="Department" asp-action="Index">Department</a>
                        </li>
                    }
                </ul>
            </div>
        </div>
    </nav>
</header>
```
## Configure IdentityOptions
We can cofigure options inside the ConfigureService method this way:
```c#
public IConfiguration Configuration { get; }

public void ConfigureServices(IServiceCollection services)
{
    services.AddDbContext<Repository>();
    services
        .AddIdentity<IdentityUser, IdentityRole>()
        .AddEntityFrameworkStores<Repository>();

    services.Configure<IdentityOptions>(options =>
    {
        //Password settings
        options.Password.RequireDigit = true;
        options.Password.RequiredLength = 8;
        options.Password.RequireNonAlphanumeric = true;
        options.Password.RequireUppercase = true;
        options.Password.RequireLowercase = true;
        options.Password.RequiredUniqueChars = 6;

        //Lockout settings
        options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
        options.Lockout.MaxFailedAccessAttempts = 10;

        //User settings
        options.User.RequireUniqueEmail = true;
    });

    services.Configure<CookiePolicyOptions>(options =>
    {
        options.CheckConsentNeeded = context => true;
        options.MinimumSameSitePolicy = SameSiteMode.None;
    });

    services.ConfigureApplicationCookie(options =>
    {
        // Cookie settings
        options.ExpireTimeSpan = TimeSpan.FromMinutes(30);
        // If the LoginPath isn't set, ASP.NET Core default is /Account/Login
        options.LoginPath = "/Account/Login";
        // If the accessDeniedPAth isn't set, ASP.NET Core default is /Account/AccessDenied
        options.AccessDeniedPath = "/Account/AccessDenied";
    });

    services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
}
```

# Authorizatioin
Initial set up:
1. Add roles to table AspNetRoles
    ```sql
    INSERT INTO AspNetRoles VALUES (NEWID(), 'Admin', 'ADMIN', NEWID())
    INSERT INTO AspNetRoles VALUES (NEWID(), 'User', 'USER', NEWID())
    INSERT INTO AspNetRoles VALUES (NEWID(), 'Guest', 'GUEST', NEWID())
    ```
2. Make one user '_Admin_': insert inside AspNetUserRoles a row with the chosen user id and the Admin role id
3. Make one user '_User_': insert inside AspNetUserRoles a row with the chosen user id and the User role id
4. Add AccessDenied page (View - Controller):
    ```html
    @{
        ViewData["Title"] = "AccessDenied";
    }

    <div class="text-center">
        <h1 class="display-4 error">Access denied!</h1>
    </div>
    ```
    ```c#
    [HttpGet]
    public ActionResult AccessDenied()
    {
        return View();
    }
    ```
5. Add ```[Authorize(Roles = "Admin, User")]``` annotation to Index action of department controller
6. Add ```[Authorize(Roles = "Admin")]``` annotation to Create action of department controller
7. Add the IsInRole condition to department action link inside the navbar
    ```html
    <div class="navbar-collapse collapse d-sm-inline-flex flex-sm-row-reverse">
        <ul class="navbar-nav flex-grow-1">
            <li class="nav-item">
                <a class="nav-link text-dark" asp-area="" asp-controller="Home" asp-action="Index">Home</a>
            </li>
            @if (!User.Identity.IsAuthenticated)
            {
                <li class="nav-item">
                    <a class="nav-link text-dark" asp-area="" asp-controller="Account" asp-action="Login">Log in</a>
                </li>
            }
            else
            {
                <li class="nav-item">
                    <a class="nav-link text-dark" asp-area="" asp-controller="Account" asp-action="Logout">Log out</a>
                </li>
                if (User.IsInRole("Admin") || User.IsInRole("User"))
                {
                    <li class="nav-item">
                        <a class="nav-link text-dark" asp-area="" asp-controller="Department" asp-action="Index">Department</a>
                    </li>
                }
            }
        </ul>
    </div>

    ```
8. Default role, edit the register post action
    ```c#
        [HttpPost]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            var user = new IdentityUser
            {
                UserName = model.UserName,
                Email = model.Email
            };
            var result = await _userManager.CreateAsync(user, model.Password);
            var resultRole = await _userManager.AddToRoleAsync(user, "User");
            if (result.Succeeded)
            {
                return RedirectToAction(nameof(Login));
            }

            return View();
        }
    ```

# Managing role dinamically
- Model
```c#
using System.Collections.Generic;

namespace ThirdASPNETCore.Models
{
    public class AssignRoleViewModel
    {
        public List<AssignRowRoleViewModel> UsersWithRole { get; set; }
        public List<string> Roles { get; set; }
    }

    public class AssignRowRoleViewModel
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Roles { get; set; }
    }
}
```
- View
```html
@model AssignRoleViewModel
@{
    ViewData["Title"] = "AssignRole";
}

<h1>Assign role</h1>
<table class="table">
    <thead class="table-dark">
        <tr>
            <th scope="col">Id</th>
            <th scope="col">User name</th>
            <th scope="col">Email</th>
            <th scope="col">Roles</th>
            <th scope="col">New Role</th>
        </tr>
    </thead>
    <tbody>
        @foreach (var user in Model.UsersWithRole)
        {
            <tr>
                <td>@user.UserId</td>
                <td>@user.UserName</td>
                <td>@user.Email</td>
                <td>@user.Roles</td>
                <td>
                    <form asp-controller="Account" asp-action="AssignRole" method="post">
                        <input type="hidden" name="UserId" asp-for="@user.UserId" />
                        <select name="RoleName" asp-for="@user.Roles" asp-items="new SelectList(Model.Roles)" class="form-control">
                            <option>Select</option>
                        </select>
                        <input type="submit" value="Assign Role" />
                    </form>
                </td>
            </tr>
        }
    </tbody>
</table>
```
- Controller
```c#
public class AccountController : Controller
{
    private readonly UserManager<IdentityUser> _userManager;
    private readonly SignInManager<IdentityUser> _signInManager;
    private readonly RoleManager<IdentityRole> _roleManager;

    public AccountController(
        UserManager<IdentityUser> userManager,
        SignInManager<IdentityUser> sigInManager,
        RoleManager<IdentityRole> roleManager
    )
    {
        _userManager = userManager;
        _signInManager = sigInManager;
        _roleManager = roleManager;
    }

//...

[HttpGet]
public async Task<ActionResult> AssignRole()
{
    var users = _userManager.Users.ToList();
    var roles = _roleManager.Roles.Select(x => x.Name).ToList();
    var model = new AssignRoleViewModel
    {
        Roles = roles,
        UsersWithRole = new List<AssignRowRoleViewModel>()
    };

    foreach (var identityUser in users)
    {
        var userRoles = string.Join(
            ";", 
            (await _userManager.GetRolesAsync(identityUser)).ToList()
        );
        model.UsersWithRole.Add(new AssignRowRoleViewModel
        {
            UserId = identityUser.Id,
            UserName = identityUser.UserName,
            Email = identityUser.Email,
            Roles = userRoles
        });
    }

    return View(model);
}

[HttpPost]
public async Task<IActionResult> AssignRole(string UserId, string RoleName)
{
    var user = await _userManager.FindByIdAsync(UserId);
    var roles = _roleManager.Roles.Select(x => x.Name).ToList();

    await _userManager.RemoveFromRolesAsync(user, roles);
    await _userManager.AddToRoleAsync(user, RoleName);

    return RedirectToAction(nameof(AssignRole));
}

//...
```
