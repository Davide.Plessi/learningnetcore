﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace ConsoleApp1
{
    public class Repository : DbContext
    {
        private const string ConnectionString = "Data Source=localhost\\SQLEXPRESS2017;" +
            "Initial Catalog=CorsoMvc;Persist Security Info=True;" +
            "User ID=sa;Password=SystemAdmin2017;";

        public DbSet<Department> Departments { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<CustomerDepartment> CustomerDepartments { get; set; }

        private static ILoggerFactory GetLoggerFactory()
        {
            IServiceCollection serviceCollection = new ServiceCollection();
            serviceCollection.AddLogging(builder =>
                builder
                    .AddDebug()
                    .AddFilter(DbLoggerCategory.Database.Command.Name, LogLevel.Information));
            return serviceCollection.BuildServiceProvider().GetService<ILoggerFactory>();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseLoggerFactory(GetLoggerFactory())
                .UseLazyLoadingProxies()
                .UseSqlServer(ConnectionString);
        }
    }

    [Table("Departments")]
    public class Department
    {
        [Key]
        [Column("Id")]
        public int Id { get; set; }

        [Column("Name")]
        [Required]
        [MaxLength(20)]
        public string Name { get; set; }

        [Column("Description")]
        [MaxLength(50)]
        [DefaultValue("No description")]
        public string Description { get; set; }

        public virtual List<Employee> Employees { get; set; }

        public virtual List<CustomerDepartment> CustomerDepartments { get; set; }
    }

    [Table("Employees")]
    public class Employee
    {
        [Key]
        [Column("Id")]
        public int Id { get; set; }

        [Column("Name")]
        [Required]
        [MaxLength(20)]
        public string Name { get; set; }

        [Column("Description")]
        [MaxLength(50)]
        public string Description { get; set; }

        [ForeignKey("Department")]
        [Column("DepartmentId")]
        public int DepartmentId { get; set; }
        public virtual Department Department { get; set; }
    }

    [Table("Customer")]
    public class Customer
    {
        [Key]
        [Column("Id")]
        public int Id { get; set; }

        [Column("Name")]
        [Required]
        [MaxLength(20)]
        public string Name { get; set; }

        [Column("Surname")]
        [MaxLength(20)]
        public string Surname { get; set; }

        public virtual List<CustomerDepartment> CustomerDepartments { get; set; }
    }

    [Table("CustomerDepartment")]
    public class CustomerDepartment
    {
        [Key]
        [Column("Id")]
        public int Id { get; set; }

        [Column("CustomerId")]
        public string CustomerId { get; set; }
        public virtual Customer Customer { get; set; }

        [Column("DepartmentId")]
        public string DepartmentId { get; set; }
        public virtual Department Department { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            //senza .UseLazyLoadingProxies() nella OnConfiguring()

            //var repository = new Repository();

            #region INSERT
            //var newDept = new Department
            //{
            //    Description = "Primo",
            //    Name = "Sviluppo"
            //};

            //repository.Departments.Add(newDept); 
            //repository.SaveChanges();
            #endregion

            #region UPDATE
            //var department = repository
            //    .Departments
            //    .FirstOrDefault();

            //if (department != null)
            //{
            //    department.Name = "New name";
            //    //repository.SaveChanges(); //Connected approach
            //    SaveDepartment(department); //Disconnected approach
            //}
            #endregion

            #region DELETE
            //var department = repository
            //    .Departments
            //    .FirstOrDefault();

            //if (department != null)
            //{
            //    repository.Remove(department);
            //    repository.SaveChanges();
            //}
            #endregion

            #region SELECT

            //using (var repository = new Repository())
            //{
            //    //Where condition con parametro
            //    var dep1 = repository
            //        .Departments
            //        .Find(1);

            //    //Where condition esplicita (converte in costante la lambda)
            //    var dep2 = repository
            //        .Departments
            //        .FirstOrDefault(x => x.Id == 2);

            //    //Where condition esplicita (converte in costante la lambda)
            //    var dep3 = repository
            //        .Departments
            //        .SingleOrDefault(x => x.Id == 3);

            //    //Query eseguita solo quando viene fatto .FirstOrDefault()
            //    var dep4 = repository
            //        .Departments
            //        .Where(x => x.Id == 4);
            //    var primoDaDep4 = dep4.FirstOrDefault();

            //    //Query eseguita in assegnamento della variabile grazie al ToList()
            //    var dep5 = repository
            //        .Departments
            //        .Where(x => x.Id == 5)
            //        .ToList();

            //    //Query eseguita in assegnamento della variabile grazie al Count()
            //    var dep6 = repository
            //        .Departments
            //        .Count(x => x.Id == 6);

            //    //IEnumerable => Where condition eseguita in memoria
            //    IEnumerable<Department> dep7 = repository
            //        .Departments;
            //    foreach (var dept in dep7.Where(x => x.Id % 2 == 0))
            //    {
            //        Console.WriteLine(dept.Name);
            //    }

            //    //IQueryable => Where condition eseguita direttaemnte sul DB
            //    IQueryable<Department> dep8 = repository
            //        .Departments;
            //    foreach (var dept in dep8.Where(x => x.Id % 2 == 0))
            //    {
            //        Console.WriteLine(dept.Name);
            //    }

            //    //IEnumerable => Where condition eseguita in memoria
            //    IEnumerable<Department> dep9 = repository
            //        .Departments;
            //    foreach (var dept in dep7.Where(x => IsEven(x.Id)))
            //    {
            //        Console.WriteLine(dept.Name);
            //    }

            //    //IQueryable => Where condition eseguita in MEMORIA perché non riesce 
            //    //a rendere costante il metodo IsEven()
            //    IQueryable<Department> dep10 = repository
            //        .Departments;
            //    foreach (var dept in dep8.Where(x => IsEven(x.Id)))
            //    {
            //        Console.WriteLine(dept.Name);
            //    } 
            //}
            #endregion

            #region Eager e Explicit Loading

            using (var repository = new Repository())
            {
                //EAGER LOADING
                Console.WriteLine("EAGER LOADING");
                var departments = repository
                    .Departments
                    .Include(x => x.Employees)
                    .ToList();
                foreach (var department in departments)
                {
                    foreach (var employee in department.Employees)
                        Console.WriteLine(employee.Name);
                }

                //EXPLICIT LOADING
                Console.WriteLine("EXPLICIT LOADING");
                var departments2 = repository
                    .Departments
                    .ToList();
                foreach (var department in departments2)
                {
                    repository.Entry(department).Collection(x => x.Employees).Load();
                    foreach (var employee in department.Employees)
                        Console.WriteLine(employee.Name);
                }


                //CON .UseLazyLoadingProxies() nella OnConfiguring()
                //E con "virtual" nelle proprietà di navigazione delle entità

                //LAZY LOADING
                Console.WriteLine("LAZY LOADING");
                var departments3 = repository
                    .Departments
                    .ToList();
                foreach (var department in departments3)
                {
                    foreach (var employee in department.Employees)
                        Console.WriteLine(employee.Name);
                }
            }
            #endregion
        }

        public static void SaveDepartment(Department dept)
        {
            var repository = new Repository();
            repository.Update(dept);
            repository.SaveChanges();
        }

        public static bool IsEven(int a)
        {
            return a % 2 == 0;
        }
    }
}
